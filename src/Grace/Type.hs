{-# LANGUAGE ApplicativeDo         #-}
{-# LANGUAGE BlockArguments        #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DeriveLift            #-}
{-# LANGUAGE DeriveTraversable     #-}
{-# LANGUAGE DerivingStrategies    #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE ImpredicativeTypes    #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE TypeOperators #-}

{-| This module stores the `Type` type representing polymorphic types and
    utilities for operating on `Type`s
-}
module Grace.Type
    ( -- * Types
      Binding(..)
    , BindingDomain
    , BindingsStore(..)
    , CapturedType(..)
    , Domain(..)
    , Domain'
    , ExistentialStore(..)
    , MonoDomain
    , Type(..)
    , Record(..)
    , RType(..)
    , Union(..)
      -- * Utilities
    -- , solveType
    -- , solveFields
    -- , solveAlternatives
    -- , typeFreeIn
    -- , fieldsFreeIn
    -- , alternativesFreeIn
    -- , substituteType
    -- , substituteFields
    -- , substituteAlternatives
    -- , substituteUnresolvedes
    , captureAsIs
    , caseDomainOf
    , eqDomain
    , fromMonotype
      -- * Pretty-printing
    , prettyRecordLabel
    , prettyCapitalLabel
    , prettyTextLiteral
    , unwrapType
    , unwrapRecord
    ) where

import Data.Bifunctor (Bifunctor(..))
import Data.List.NonEmpty (NonEmpty(..))
import Data.Text (Text)
import GHC.Generics (Generic)
-- import Grace.Domain (Domain)
import Grace.Existential (Existential)
import Grace.Pretty (Pretty(..), builtin, keyword, label, operator, punctuation)
import Language.Haskell.TH.Syntax (Lift (..), unsafeCodeCoerce)
import Prettyprinter (Doc)
import Prettyprinter.Render.Terminal (AnsiStyle)

import Grace.Monotype
    ( Monotype
    , RemainingAlternatives(..)
    , RemainingFields(..)
    , Scalar(..)
    , VariableId
    )

import qualified Data.List.NonEmpty as NonEmpty
import qualified Data.Text as Text
import qualified Grace.Lexer as Lexer
import qualified Grace.Monotype as Monotype
import qualified Prettyprinter as Pretty
import Data.Maybe (fromMaybe)
import Data.Hashable (Hashable (..))

-- TODO: Remove s from Type?

{- $setup

   >>> :set -XOverloadedStrings
   >>> :set -XTypeApplications
-}

data Domain t f a v where
    DomainType :: Domain t f a t
    DomainFields :: Domain t f a f
    DomainAlternatives :: Domain t f a a

caseDomainOf :: Domain t f a v -> (t ~ v => r) -> (f ~ v => r) -> (a ~ v => r) -> r
caseDomainOf d f t v = case d of
    DomainType -> f
    DomainFields -> t
    DomainAlternatives -> v

eqDomain :: Domain t f a v1 -> Domain t f a v2 -> (v1 ~ v2 => r) -> Maybe r
eqDomain d1 d2 f = case (d1, d2) of
    (DomainType, DomainType) -> Just f
    (DomainFields, DomainFields) -> Just f
    (DomainAlternatives, DomainAlternatives) -> Just f
    _domainMismatch -> Nothing

instance Eq (Domain t f a v) where
    DomainType == DomainType = True
    DomainFields == DomainFields = True
    DomainAlternatives == DomainAlternatives = True
    _ == _ = False
instance Hashable (Domain t f a v) where
    hashWithSalt salt d = hashWithSalt @Int salt $ case d of
        DomainType -> 0
        DomainFields -> 1
        DomainAlternatives -> 2

instance Show (Domain t f a v) where
    show = \case
        DomainType -> "Type"
        DomainFields -> "Fields"
        DomainAlternatives -> "Alternatives"
instance Pretty (Domain t f a v) where
    pretty = builtin . pretty . show
instance Lift (Domain t f a v) where
    liftTyped = unsafeCodeCoerce . lift

type MonoDomain a = Domain Monotype Monotype.Record Monotype.Union a
type BindingDomain s a = Domain (Type s) (Record s) (Union s) a
type Domain' = Domain () () () ()

data Binding s = forall a. Binding (BindingDomain s a) a
instance Eq s => Eq (Binding s) where
    (Binding d1 v1) == (Binding d2 v2) = fromMaybe False $ eqDomain d1 d2 $
        caseDomainOf d1 (==) (==) (==) v1 v2

instance Functor Binding where
    fmap f (Binding d v) = case d of
        DomainType -> Binding DomainType (f <$> v)
        DomainFields -> Binding DomainFields (f <$> v)
        DomainAlternatives -> Binding DomainAlternatives (f <$> v)
instance Lift (Binding s) where
    liftTyped = unsafeCodeCoerce . lift -- No... I don't know if this is enough...
instance Show s => Show (Binding s) where
    show (Binding d v) = caseDomainOf d show show show v

-- | A potentially polymorphic type
data Type s
    = VariableType { location :: s, existential :: Existential Monotype }
    -- ^ Type variable
    --
    -- >>> pretty @(Type ()) (VariableType () "a")
    -- a
    | UnsolvedType { location :: s, existential :: Existential Monotype }
    -- ^ A placeholder variable whose type has not yet been inferred
    --
    -- >>> pretty @(Type ()) (UnsolvedType () 0)
    -- a?

    | Exists { location :: s, nameLocation :: s, name :: VariableId, domain :: Domain', type_ :: Type s }
    -- ^ Existentially quantified type
    --
    -- >>> pretty @(Type ()) (Exists () () "a" Domain.Type "a")
    -- exists (a : Type) . a
    | Forall { location :: s, nameLocation :: s, name :: VariableId, domain :: Domain', type_ :: Type s }
    -- ^ Universally quantified type
    --
    -- >>> pretty @(Type ()) (Forall () () "a" Domain.Type "a")
    -- forall (a : Type) . a
    | Assign { location :: s, name :: VariableId, val :: Binding s, type_ :: Type s }
    | Function { location :: s, input :: Type s, output :: Type s }
    -- ^ Function type
    --
    -- >>> pretty @(Type ()) (Function () "a" "b")
    -- a -> b
    | Optional { location :: s, type_ :: Type s }
    -- ^ Optional type
    --
    -- >>> pretty @(Type ()) (Optional () "a")
    -- Optional a
    | List { location :: s, type_ :: Type s }
    -- ^ List type
    --
    -- >>> pretty @(Type ()) (List () "a")
    -- List a
    | Record { location :: s, fields :: Record s }
    -- ^ Record type
    --
    -- >>> pretty @(Type ()) (Record () (Fields [("x", "X"), ("y", "Y")] Monotype.EmptyFields))
    -- { x: X, y: Y }
    -- >>> pretty @(Type ()) (Record () (Fields [("x", "X"), ("y", "Y")] (Monotype.UnsolvedFields 0)))
    -- { x: X, y: Y, a? }
    | Union { location :: s, alternatives :: Union s }
    -- ^ Union type
    --
    -- >>> pretty @(Type ()) (Union () (Alternatives [("X", "X"), ("Y", "Y")] Monotype.EmptyAlternatives))
    -- < X: X | Y: Y >
    -- >>> pretty @(Type ()) (Union () (Alternatives [("X", "X"), ("Y", "Y")] (Monotype.UnsolvedAlternatives 0)))
    -- < X: X | Y: Y | a? >
    | Unresolved { location :: s, name :: VariableId }
    | Scalar { location :: s, scalar :: Scalar }
    deriving stock (Eq, Functor, Generic, Lift, Show)

-- data Bindings s = Bindings {
--         b_values :: [(Text, Maybe (RType s))], 
--         b_types :: [(VariableId, RType s)],
--         b_fields :: [(VariableId, Monotype.RemainingFields)],
--         b_alternatives :: [(VariableId, Monotype.RemainingAlternatives)]
--     }

-- | Use context 'f' to read assignment of fields/alternatives type variable from 'store'.
-- Use context 'f' to unwrap existential, possibly acquiring its value and 'store'.
class Monad f => ExistentialStore f store where
    unwrapEx :: MonoDomain a -> Existential a -> f (Maybe (store, a))

-- | Use context 'f' to read type variable.
-- Type variables are typically instantiated to 'Monotype's by typechecker,
-- but it's possible that they are assigned to 'Type' due to use of manually annotated
-- variables or type values.
class ExistentialStore f store => BindingsStore f store s where
    bGet :: s -> BindingDomain s a -> VariableId -> store -> f (Maybe (store, a))
    bSet :: BindingDomain s a -> VariableId -> store -> a -> f store -- f is not required here

-- | 'RType store s' ~ '(store, Type s)', but is heavily used in typechecking
-- so deserves a distinct type.
data RType store s = RType store (Type s)
-- FARENDE: location

unwrapType :: (Monad f, BindingsStore f store s) => RType store s -> f (store, Type s)
unwrapType (RType ctx0 type0) = case type0 of
    UnsolvedType {..} -> unwrapEx DomainType existential >>= \case
        Nothing -> pure (ctx0, type0)
        Just (ctx, m) -> unwrapType $ RType ctx (location <$ fromMonotype m)
    Unresolved {..} -> bGet location DomainType name ctx0 >>= \case
        Nothing -> pure (ctx0, type0)
        Just upd -> unwrapType $ uncurry RType upd
    Record { fields = fields0, .. } -> (fmap \fields -> Record { .. }) <$> unwrapRecord location ctx0 fields0
    Union { alternatives = alternatives0, .. } -> (fmap \alternatives -> Union { .. }) <$> unwrapUnion location ctx0 alternatives0
    Assign { val = Binding d val, .. } -> do
        ctx <- bSet d name ctx0 val
        unwrapType (RType ctx type_)
    _unwrapped -> pure (ctx0, type0)

-- FARENDE: It's imperative to unwrap the entirity of record/union
unwrapRecord :: BindingsStore f store s => s -> store -> Record s -> f (store, Record s)
unwrapRecord location ctx0 r0@(Fields fields0 remaining0) = case remaining0 of
    Left name -> bGet location DomainFields name ctx0 >>= \case
        Nothing -> pure (ctx0, r0)
        Just (ctx, Fields fields1 r1) -> unwrapRecord location ctx (Fields (fields0 <> fields1) r1)
    Right (UnsolvedFields existential) -> unwrapEx DomainFields existential >>= \case
        Nothing -> pure (ctx0, r0)
        Just (ctx, Monotype.Fields fieldsExt remaining) ->
            unwrapRecord location ctx $ 
                Fields (fields0 <> fmap (fmap (fmap (const location) . fromMonotype)) fieldsExt) (Right remaining)
    Right _nonUnsolved -> pure (ctx0, r0)

unwrapUnion :: BindingsStore f store s => s -> store -> Union s -> f (store, Union s)
unwrapUnion location ctx0 r0@(Alternatives alternatives0 remaining0) = case remaining0 of
    Left name -> bGet location DomainAlternatives name ctx0 >>= \case 
        Nothing -> pure (ctx0, r0)
        Just (ctx, Alternatives alternatives1 r1) -> unwrapUnion location ctx (Alternatives (alternatives0 <> alternatives1) r1)
    Right (UnsolvedAlternatives existential) -> unwrapEx DomainAlternatives existential >>= \case
        Nothing -> pure (ctx0, r0)
        Just (ctx, Monotype.Alternatives alternativesExt remaining) ->
            unwrapUnion location ctx $ 
                Alternatives (alternatives0 <> fmap (fmap (fmap (const location) . fromMonotype)) alternativesExt) (Right remaining)
    Right _nonUnsolved -> pure (ctx0, r0)


-- data Baked store s f = DomainType (forall a. f a -> a) store (Type s)
data CapturedType t s where
    CapturedType :: BindingsStore f store s => (forall a. f a -> Maybe a) -> store -> t s -> CapturedType t s
instance Show (t s) => Show (CapturedType t s) where
    show (CapturedType _ _ t) = show t
instance Eq (t s) => Eq (CapturedType t s) where
    (CapturedType _ _ t1) == (CapturedType _ _ t2) = t1 == t2

-- TODO: Use Nothing instead of failures
data Unit1 a = Unit1
instance Functor Unit1 where
    fmap _ _ = Unit1
instance Applicative Unit1 where
    pure _ = Unit1
    _ <*> _ = Unit1
instance Monad Unit1 where
    _ >>= _ = Unit1
instance ExistentialStore Unit1 () where
  unwrapEx _ _ = Unit1
instance BindingsStore Unit1 () s where
  bGet _ _ _ _ = Unit1
  bSet _ _ _ _ = Unit1

-- | Useful for printing 'Type' "as is", without unwrapping inner variables/existentials
-- FARENDE:
captureAsIs :: t s -> CapturedType t s
captureAsIs = CapturedType (\Unit1 -> Nothing) ()

-- unwrapCapturedType :: CapturedType Type s -> (forall t. t s -> CapturedType t s, Type s)
-- unwrapCapturedType (CapturedType run ctx0 type0) =
--     let (ctx, type_) = run $ unwrapType $ RType ctx0 type0
--     in (CapturedType run ctx, type_)

-- дайте мне просто сдохнуть

-- infixr 5 :@

-- instance IsString (Type ()) where
--     fromString string = VariableType{ name = fromString string, location = () }

instance Pretty (CapturedType Type s) where
    pretty = pretty . renderType

-- instance Plated (Type s) where
--     plate onType type_ =
--         case type_ of
--             VariableType{..} -> do
--                 pure VariableType{..}
--             UnsolvedType{..} -> do
--                 pure UnsolvedType{..}
--             Exists{ type_ = oldType, .. }-> do
--                 newType <- onType oldType
--                 return Exists{ type_ = newType, .. }
--             Forall{ type_ = oldType, .. } -> do
--                 newType <- onType oldType
--                 return Forall{ type_ = newType, .. }
--             Function{ input = oldInput, output = oldOutput, .. } -> do
--                 newInput <- onType oldInput
--                 newOutput <- onType oldOutput
--                 return Function{ input = newInput, output = newOutput, .. }
--             Optional{ type_ = oldType, .. } -> do
--                 newType <- onType oldType
--                 return Optional{ type_ = newType, .. }
--             List{ type_ = oldType, .. } -> do
--                 newType <- onType oldType
--                 return List{ type_ = newType, .. }
--             Record{ fields = Fields oldFieldTypes remainingFields, .. } -> do
--                 let onPair (field, oldType) = do
--                         newType <- onType oldType
--                         return (field, newType)
--                 newFieldTypes <- traverse onPair oldFieldTypes
--                 return Record{ fields = Fields newFieldTypes remainingFields, .. }
--             Union{ alternatives = Alternatives oldAlternativeTypes remainingAlternatives, .. } -> do
--                 let onPair (alternative, oldType) = do
--                         newType <- onType oldType
--                         return (alternative, newType)
--                 newAlternativeTypes <- traverse onPair oldAlternativeTypes
--                 return Union{ alternatives = Alternatives newAlternativeTypes remainingAlternatives, .. }
--             Unresolved { parameters = oldParameters, .. } -> do
--                 newParameters <- traverse onType oldParameters
--                 return Unresolved { parameters = newParameters, .. }
--             Scalar{..} -> do
--                 pure Scalar{..}

-- | A potentially polymorphic record type
data Record s = Fields [(Text, Type s)] (Either VariableId RemainingFields)
    deriving stock (Eq, Functor, Generic, Lift, Show)

instance Pretty (CapturedType Record s) where
    pretty = prettyRecordType

-- | A potentially polymorphic union type
data Union s = Alternatives [(Text, Type s)] (Either VariableId RemainingAlternatives)
    deriving stock (Eq, Functor, Generic, Lift, Show)

instance Pretty (CapturedType Union s) where
    pretty = prettyUnionType

{-| This function should not be exported or generally used because it does not
    handle the `location` field correctly.  It is only really safe to use within
    one of the @solve*@ functions
-}
fromMonotype :: Monotype -> Type ()
fromMonotype monotype =
    case monotype of
        Monotype.VariableType existential ->
            VariableType{..}
        Monotype.UnsolvedType existential ->
            UnsolvedType{..}
        Monotype.Function input output ->
            Function{ input = fromMonotype input, output = fromMonotype output, .. }
        Monotype.Optional type_ ->
            Optional{ type_ = fromMonotype type_, .. }
        Monotype.List type_ ->
            List{ type_ = fromMonotype type_, .. }
        Monotype.Record (Monotype.Fields kτs ρ) ->
            Record{ fields = Fields (map (second fromMonotype) kτs) (Right ρ), .. }
        Monotype.Union (Monotype.Alternatives kτs ρ) ->
            Union{ alternatives = Alternatives (map (second fromMonotype) kτs) (Right ρ), .. }
        Monotype.Scalar scalar ->
            Scalar{..}
  where
    location = ()

-- instance Pretty Monotype where
--     pretty = pretty . fromMonotype

{-| Substitute a `Type` by replacing all occurrences of the given unsolved
    variable with a `Monotype`
-}
-- solveType :: Existential Monotype -> Monotype -> Type s -> Type s
-- solveType unsolved monotype = Lens.transform transformType
--   where
--     transformType UnsolvedType {..}
--         | unsolved == existential =
--             fmap (\_ -> location) (fromMonotype monotype)

--     transformType type_ =
--         type_

{-| Substitute a `Type` by replacing all occurrences of the given unsolved
    fields variable with a t`Monotype.Record`
-}
-- solveFields
--     :: Existential Monotype.Record -> Monotype.Record -> Type s -> Type s
-- solveFields unsolved (Monotype.Fields fieldMonotypes fields) =
--     Lens.transform transformType
--   where
--     transformType Record{ fields = Fields fieldTypes (Right (UnsolvedFields existential)), .. }
--         | unsolved == existential =
--             Record{ fields = Fields fieldTypes' (Right fields), .. }
--       where
--         fieldTypes' =
--             fieldTypes <> map transformPair fieldMonotypes

--         transformPair (field, monotype) =
--             (field, fmap (\_ -> location) (fromMonotype monotype))

--     transformType type_ =
--         type_

{-| Substitute a `Type` by replacing all occurrences of the given unsolved
    alternatives variable with a t`Monotype.Union`
-}
-- solveAlternatives
--     :: Existential Monotype.Union -> Monotype.Union -> Type s -> Type s
-- solveAlternatives unsolved (Monotype.Alternatives alternativeMonotypes alternatives) =
--     Lens.transform transformType
--   where
--     transformType Union{ alternatives = Alternatives alternativeTypes (Right (UnsolvedAlternatives existential)), .. }
--         | unsolved == existential =
--             Union{ alternatives = Alternatives alternativeTypes' (Right alternatives), .. }
--       where
--         alternativeTypes' =
--             alternativeTypes <> map transformPair alternativeMonotypes

--         transformPair (alternative, monotype) =
--             (alternative, fmap (\_ -> location) (fromMonotype monotype))

--     transformType type_ =
--         type_

{-| Helper function for traversing the tree during `Type` substitutions.
-}
-- substitute :: VariableId -> Domain -> (Type s -> Type s) -> Type s -> Type s
-- substitute a aDomain substituter = sub
--     where
--         sub x =
--             let result = substituter x
--             in (plate . Lens.filtered allowed %~ sub) result

--         allowed Exists{ .. }
--             | a == name && domain == aDomain = False
--         allowed Forall{ .. }
--             | a == name && domain == aDomain = False
--         allowed _ = True

{-| Replace all occurrences of a variable within one `Type` with another `Type`,
    given the variable's id
-}
-- substituteType :: VariableId -> Type s -> Type s -> Type s
-- substituteType a _A = substitute a Domain.Type \case
--     VariableType {..}
--         | a == name -> _A
--     v -> v

{-| Replace all occurrences of a variable within one `Type` with another `Type`,
    given the variable's id
-}
-- substituteFields :: VariableId -> Record s -> Type s -> Type s
-- substituteFields ρ0 (Fields kτs ρ1) = substitute ρ0 Domain.Fields \case
--     Record{ fields = Fields kAs0 (Right ρ), .. }
--         | VariableFields ρ0 == ρ ->
--             Record{ fields = Fields kAs1 ρ1, .. }
--       where
--         kAs1 = kAs0 <> map (second (fmap (\_ -> location))) kτs
--     v -> v

{-| Replace all occurrences of a variable within one `Type` with another `Type`,
    given the variable's id
-}
-- substituteAlternatives :: VariableId -> Union s -> Type s -> Type s
-- substituteAlternatives ρ0 (Alternatives kτs ρ1) = substitute ρ0 Domain.Alternatives \case
--     Union{ alternatives = Alternatives kAs0 (Right ρ), .. }
--         | Monotype.VariableAlternatives ρ0 == ρ ->
--             Union{ alternatives = Alternatives kAs1 ρ1, .. }
--       where
--         kAs1 = kAs0 <> map (second (fmap (\_ -> location))) kτs
--     v -> v

-- {-| Resolve all occurences of `Unresolved`s within the `Type`,
--     given predicate
-- -}
-- substituteUnresolved :: Monad f
--     => (Text -> [Type s] -> f (Type s))
--     -> (Text -> f RemainingFields)
--     -> (Text -> f RemainingAlternatives)
--     -> Type s
--     -> f (Type s)
-- substituteUnresolved subType subFields subAlternatives = Lens.transformM substituter
--     where
--         substituter (Unresolved {..}) = subType rawName parameters
--         substituter (Forall ...) = ...
--         substituter orig@(Record { fields = Fields f r0, .. }) = case r0 of
--             Left n -> do
--                 r <- subFields n
--                 pure $ Record { fields = Fields f (Right r), .. }
--             Right _ -> pure orig
--         substituter orig@(Union { alternatives = Alternatives a r0, .. }) = case r0 of
--             Left n -> do
--                 r <- subAlternatives n
--                 pure $ Union { alternatives = Alternatives a (Right r), .. }
--             Right _ -> pure orig
--         substituter x = pure x

{-| Count how many times the given `Existential` `Type` variable appears within
    a `Type`
-}
-- typeFreeIn :: Existential Monotype -> Type s -> Bool
-- typeFreeIn unsolved =
--     Lens.has
--         ( Lens.cosmos
--         . _As @"UnsolvedType"
--         . the @2
--         . Lens.only unsolved
--         )

{-| Count how many times the given `Existential` t`Monotype.Record` variable
    appears within a `Type`
-}
-- fieldsFreeIn :: Existential Monotype.Record -> Type s -> Bool
-- fieldsFreeIn unsolved =
--     Lens.has
--         ( Lens.cosmos
--         . _As @"Record"
--         . the @2
--         . the @2
--         . Lens._Right
--         . _As @"UnsolvedFields"
--         . Lens.only unsolved
--         )

{-| Count how many times the given `Existential` t`Monotype.Union` variable
    appears within a `Type`
-}
-- alternativesFreeIn :: Existential Monotype.Union -> Type s -> Bool
-- alternativesFreeIn unsolved =
--     Lens.has
--         ( Lens.cosmos
--         . _As @"Union"
--         . the @2
--         . the @2
--         . Lens._Right
--         . _As @"UnsolvedAlternatives"
--         . Lens.only unsolved
--         )

data RenderQuantifier = RForall | RExists
    deriving Eq
data RenderType
    = RQuantified RenderQuantifier (NonEmpty (Domain', VariableId)) RenderType
    | RFunction (NonEmpty RenderType) RenderType
    | RApplication (Doc AnsiStyle) (NonEmpty RenderType)
    | RPrimitive (Doc AnsiStyle)

renderType :: forall s. CapturedType Type s -> RenderType
renderType (CapturedType run ctx0 type0) =
    -- unwrapCapturedType (CapturedType run ctx0 type0) =
    --     let (ctx, type_) = run $ unwrapType $ RType ctx0 type0
    --     in (CapturedType run ctx, type_)
    let (ctx, type1) = fromMaybe (ctx0, type0) $ run $ unwrapType $ RType ctx0 type0
        f :: forall t. t s -> CapturedType t s
        f = CapturedType run ctx in
    case type1 of
        VariableType {..} -> RPrimitive $ label (pretty existential)
        UnsolvedType {..} -> RPrimitive $ label (pretty existential <> "?")
        Exists       {..} -> RQuantified RExists ((domain, name) :| []) $ renderType $ f type_
        Forall       {..} -> RQuantified RForall ((domain, name) :| []) $ renderType $ f type_
        Function     {..} -> RFunction (renderType (f input) :| []) $ renderType $ f output
        Optional     {..} -> RApplication "Optional" (renderType (f type_) :| [])
        List         {..} -> RApplication "List" (renderType (f type_) :| [])
        Record       {..} -> RPrimitive $ prettyRecordType $ f fields
        Union        {..} -> RPrimitive $ prettyUnionType $ f alternatives
        Unresolved   {..} -> RPrimitive (pretty name)
        Scalar       {..} -> RPrimitive $ pretty scalar
        Assign       {..} -> renderType $ f type_

renderPrecedence :: RenderType -> Int
renderPrecedence (RQuantified  {}) = 4
renderPrecedence (RFunction    {}) = 3
renderPrecedence (RApplication {}) = 2
renderPrecedence (RPrimitive   {}) = 1

prettySub :: RenderType -> RenderType -> Doc AnsiStyle
prettySub (RQuantified {}) sub@(RQuantified {}) = pretty sub
prettySub orig sub
    | renderPrecedence orig > renderPrecedence sub
        = pretty sub
    | otherwise =
        Pretty.group (Pretty.flatAlt (Pretty.align $ prettyAll True) (prettyAll False))
  where
    prettyAll long =
            punctuation "("
        <>  (if long then " " else mempty)
        <>  pretty sub
        <>  (if long then Pretty.hardline else mempty)
        <>  punctuation ")"

instance Pretty RenderType where
    pretty (RQuantified q1 l1 (RQuantified q2 l2 body))
        | q1 == q2 = pretty $ RQuantified q1 (l1 <> l2) body
    pretty (RFunction i1 (RFunction i2 output))
        = pretty $ RFunction (i1 <> i2) output

    pretty orig@(RQuantified q l body) =
        Pretty.group (Pretty.flatAlt (Pretty.align $ prettyAll True) (prettyAll False))
      where
        start = keyword (case q of
            RForall -> "forall"
            RExists -> "exists") <> " "
        prettyAll long =
                    (if long then mempty else start)
                <>  mconcat (NonEmpty.toList (pretty1 long <$> l))
                <>  (if long then "  " else punctuation "." <> " ")
                <>  prettySub orig body
        pretty1 long (domain, name) =
                (if long then start else mempty)
            <>  punctuation "("
            <>  label (pretty name)
            <>  " "
            <>  punctuation ":"
            <>  " "
            <>  pretty domain
            <>  punctuation ")"
            <>  " "
            <>  (if long then punctuation "." <> Pretty.hardline else mempty)

    pretty orig@(RFunction i o) =
        Pretty.group (Pretty.flatAlt (Pretty.align $ prettyAll True) (prettyAll False))
      where
        prettyAll long =
            let sep = " " <> punctuation "->" <> (if long then Pretty.hardline else " ")
                subs = (prettySub orig <$> i) <> (((if long then "  " else mempty) <> prettySub orig o) :| [])
            in mconcat $ NonEmpty.toList (NonEmpty.intersperse sep subs)

    pretty orig@(RApplication name subs)
        = Pretty.group (Pretty.flatAlt (Pretty.align $ prettyAll True) (prettyAll False))
      where
        prettyAll long =
                builtin (pretty name)
            <>  (if long then Pretty.hardline <> "  " else " ")
            <>  mconcat (NonEmpty.toList (NonEmpty.intersperse " " (prettySub orig <$> subs)))

    pretty (RPrimitive x) = x

prettyRecordType :: CapturedType Record s -> Doc AnsiStyle
prettyRecordType (CapturedType run ctx0 record0) =
    let (ctx, record) = fromMaybe (ctx0, record0) $ run $ unwrapRecord (error "3am at the night. I'm tired and worthless. This is impossible.") ctx0 record0
        wrap = CapturedType run ctx
    in case record of
        Fields [] fields ->
                punctuation "{"
            <>  " "
            <>  (case fields of
                    Left e -> "\"" <> pretty e <> "\" "
                    Right fields' -> case fields' of
                        EmptyFields      -> mempty
                        UnsolvedFields ρ -> label (pretty ρ <> "?") <> " "
                        VariableFields ρ -> label (pretty ρ) <> " "
                )
            <>  punctuation "}"
        Fields (keyType : keyTypes) fields ->
            let
                short =
                        punctuation "{"
                    <>  " "
                    <>  prettyShortFieldType keyType
                    <>  foldMap (\ft -> punctuation "," <> " " <> prettyShortFieldType ft) keyTypes
                    <>  (case fields of
                            Left e -> "\"" <> pretty e <> "\""
                            Right fields' -> case fields' of
                                EmptyFields ->
                                    mempty
                                UnsolvedFields ρ ->
                                    punctuation "," <> " " <> label (pretty ρ <> "?")
                                VariableFields ρ ->
                                    punctuation "," <> " " <> label (pretty ρ)
                        )
                    <>  " "
                    <>  punctuation "}"

                long =
                    Pretty.align
                        (   punctuation "{"
                        <>  " "
                        <>  prettyLongFieldType keyType
                        <>  foldMap (\ft -> punctuation "," <> " " <> prettyLongFieldType ft) keyTypes
                        <>  (case fields of
                                Left x ->
                                        punctuation ","
                                    <> " \""
                                    <> label (pretty x)
                                    <> "\""
                                    <> Pretty.hardline
                                Right alternatives' -> case alternatives' of
                                    EmptyFields -> mempty
                                    UnsolvedFields ρ ->
                                            punctuation ","
                                        <>  " "
                                        <>  label (pretty ρ <> "?")
                                        <>  Pretty.hardline
                                    VariableFields ρ ->
                                            punctuation ","
                                        <>  " "
                                        <>  label (pretty ρ)
                                        <>  Pretty.hardline)
                        <> punctuation "}"
                        )

                prettyShortFieldType (key, type_) =
                        prettyRecordLabel False key
                    <>  operator ":"
                    <>  " "
                    <>  pretty (wrap type_)

                prettyLongFieldType (key, type_) =
                        prettyRecordLabel False key
                    <>  operator ":"
                    <>  Pretty.group (Pretty.flatAlt (Pretty.hardline <> "    ") " ")
                    <>  pretty (wrap type_)
                    <>  Pretty.hardline
            in Pretty.group (Pretty.flatAlt long short)

prettyUnionType :: CapturedType Union s -> Doc AnsiStyle
prettyUnionType (CapturedType run ctx0 union0) =
    let (ctx, union) = fromMaybe (ctx0, union0) $ run $ unwrapUnion (error "3am at the night. I'm tired and worthless. This is impossible.") ctx0 union0
        wrap = CapturedType run ctx
    in case union of
        Alternatives [] alternatives ->
                punctuation "<"
            <>  " "
            <>  (case alternatives of
                    Left x -> "\"" <> pretty x <> "\""
                    Right alternatives' -> case alternatives' of
                        EmptyAlternatives      -> mempty
                        UnsolvedAlternatives ρ -> label (pretty ρ <> "?") <> " "
                        VariableAlternatives ρ -> label (pretty ρ) <> " "
                )
            <> punctuation ">"
        Alternatives (keyType : keyTypes) alternatives ->
            let
                short =
                        punctuation "<"
                    <>  " "
                    <>  prettyShortAlternativeType keyType
                    <>  foldMap (\kt -> " " <> punctuation "|" <> " " <> prettyShortAlternativeType kt) keyTypes
                    <>  (case alternatives of
                            Left x -> " " <> punctuation "|" <> " \"" <> pretty x <> "\""
                            Right alternatives' -> case alternatives' of
                                EmptyAlternatives ->
                                    mempty
                                UnsolvedAlternatives ρ ->
                                        " "
                                    <>  punctuation "|"
                                    <>  " "
                                    <>  label (pretty ρ <> "?")
                                VariableAlternatives ρ ->
                                        " "
                                    <>  punctuation "|"
                                    <>  " "
                                    <>  label (pretty ρ)
                        )
                    <>  " "
                    <>  punctuation ">"

                long  =
                    Pretty.align
                        (   punctuation "<"
                        <>  " "
                        <>  prettyLongAlternativeType keyType
                        <>  foldMap (\kt -> punctuation "|" <> " " <> prettyLongAlternativeType kt) keyTypes
                        <>  case alternatives of
                                Left x ->
                                        punctuation "|"
                                    <> " \""
                                    <> label (pretty x)
                                    <> "\""
                                    <> Pretty.hardline
                                    <> punctuation ">"
                                Right alternatives' -> case alternatives' of
                                    EmptyAlternatives ->
                                        punctuation ">"
                                    UnsolvedAlternatives ρ ->
                                            punctuation "|"
                                        <>  " "
                                        <>  label (pretty ρ <> "?")
                                        <>  Pretty.hardline
                                        <>  punctuation ">"
                                    VariableAlternatives ρ ->
                                            punctuation "|"
                                        <>  " "
                                        <>  label (pretty ρ)
                                        <>  Pretty.hardline
                                        <>  punctuation ">"
                        )

                prettyShortAlternativeType (key, type_) =
                        prettyCapitalLabel key
                    <>  operator ":"
                    <>  " "
                    <>  pretty (wrap type_)

                prettyLongAlternativeType (key, type_) =
                        prettyCapitalLabel key
                    <>  operator ":"
                    <>  Pretty.group (Pretty.flatAlt (Pretty.hardline <> "    ") " ")
                    <>  pretty (wrap type_)
                    <>  Pretty.hardline
            in  Pretty.group (Pretty.flatAlt long short)

-- | Pretty-print a @Text@ literal
prettyTextLiteral :: Text -> Doc AnsiStyle
prettyTextLiteral text =
        "\""
    <>  ( pretty
        . Text.replace "\"" "\\\""
        . Text.replace "\b" "\\b"
        . Text.replace "\f" "\\f"
        . Text.replace "\n" "\\n"
        . Text.replace "\r" "\\r"
        . Text.replace "\t" "\\t"
        . Text.replace "\\" "\\\\"
        ) text
    <>  "\""

-- | Pretty-print a @Text@ literal
prettyQuotedAlternative :: Text -> Doc AnsiStyle
prettyQuotedAlternative text =
        "'"
    <>  ( pretty
        . Text.replace "'" "\\\'"
        . Text.replace "\b" "\\b"
        . Text.replace "\f" "\\f"
        . Text.replace "\n" "\\n"
        . Text.replace "\r" "\\r"
        . Text.replace "\t" "\\t"
        . Text.replace "\\" "\\\\"
        ) text
    <>  "'"

-- | Pretty-print a record label
prettyRecordLabel
    :: Bool
    -- ^ Always quote the label if `True`
    --
    -- This is mainly set to `True` when pretty-printing records so that the
    -- output is valid JSON
    -> Text
    -> Doc AnsiStyle
prettyRecordLabel alwaysQuote field
    | Lexer.validRecordLabel field && not alwaysQuote =
        label (pretty field)
    | otherwise =
        label (prettyTextLiteral field)

-- | Pretty-print an alternative label
prettyCapitalLabel
    :: Text
    -> Doc AnsiStyle
prettyCapitalLabel alternative
    | Lexer.validCapitalLabel alternative =
        label (pretty alternative)
    | otherwise =
        label (prettyQuotedAlternative alternative)
