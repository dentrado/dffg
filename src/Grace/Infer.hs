{-# LANGUAGE BlockArguments    #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE MultiWayIf        #-}
{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE TypeApplications  #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE RankNTypes #-}

{-| This module is based on the bidirectional type-checking algorithm from:

    Dunfield, Jana, and Neelakantan R. Krishnaswami. \"Complete and easy bidirectional typechecking for higher-rank polymorphism.\" ACM SIGPLAN Notices 48.9 (2013): 429-442.

    The main differences from the original algorithm are:

    * This uses `Control.Monad.State.Strict.StateT` to thread around
      `Context`s and manipulate them instead of explicit `Context` passing as
      in the original paper

    * This algorithm adds support for existential quantification

    * This algorithm adds support for row polymorphic and polymorphic variants
-}
module Grace.Infer
    ( -- * Type inference
      typeWith
    , typeOf
      -- * Errors related to type inference
    , TypeInferenceError(..)
    , TypeInferenceErrorS(..)
    ) where

import Control.Applicative ((<|>))
import Control.Exception.Safe (Exception(..))
import Control.Lens (view, (%~), (.~))
import Control.Lens.Lens (Lens', lens)
import Control.Monad.Except (MonadError(..), runExcept)
import Control.Monad.Reader (MonadReader, MonadTrans (lift), ReaderT)
import Control.Monad.State.Strict (MonadState, StateT)
import Data.Foldable (traverse_)
import Data.Sequence (ViewL(..))
import Data.Text (Text)
import Data.Void (Void)
import Grace.Context (Context(..), Entry)
import Grace.Existential (Existential (..))
import Grace.Location (Location(..))
import Grace.Monotype (Monotype, VariableId(..))
import Grace.Pretty (Pretty(..), Doc, AnsiStyle)
import Grace.Syntax (Syntax)
import Grace.Type (Type(..), BindingsStore (..), RType(..), ExistentialStore, CapturedType (..), BindingDomain, Domain (..), Domain')
import Grace.Value (Value)

import qualified Control.Monad as Monad
import qualified Control.Monad.State.Strict as State
import qualified Data.List as List
import qualified Data.Map as Map
import qualified Data.List.NonEmpty as NonEmpty
import qualified Data.Sequence as Seq
import qualified Data.Text as Text
import qualified Grace.Context as Context
import qualified Grace.Location as Location
import qualified Grace.Monotype as Monotype
import qualified Grace.Normalize as Normalize
import qualified Grace.Pretty
import qualified Grace.Syntax as Syntax
import qualified Grace.Type as Type
import qualified Grace.Width as Width
import qualified Prettyprinter as Pretty
import qualified Control.Monad.Reader as Reader
import Debug.Trace (traceM)
import Data.Set (Set)
import Data.Map.Strict (Map)
import Data.HashMap.Strict (HashMap)
import Control.Monad.State.Strict (State)
import Data.Functor.Compose (Compose(..))
import qualified Data.HashMap.Strict as HM
import Data.Functor ((<&>))
import Data.Foldable (foldl')
import qualified Data.Set as Set
import Data.Maybe (fromMaybe)
import Data.Traversable (for, forM)
import Control.Monad (join)

{-
REGRESSIONS

* (3 : exists (a : Type). a)
* let f : exists (a : Type). a -> a = \x -> x+1 in \x -> f x
-}

-- | Debug stack
data Tree a = Tree a ![Tree a]
data StackZipper a = StackZipper ![Tree a] !(Maybe (a, StackZipper a))

pushStack :: a -> StackZipper a -> StackZipper a
pushStack x (StackZipper l parent) = StackZipper (Tree x []:l) parent

scopeStack :: a -> StackZipper a -> StackZipper a
scopeStack scopeName parent = StackZipper [] $ Just (scopeName, parent)

unscopeStack :: StackZipper a -> StackZipper a
unscopeStack (StackZipper lNew (Just (scopeName, StackZipper lPrev parent))) =
    StackZipper (Tree scopeName lNew:lPrev) parent
unscopeStack v = v

-- | Type-checking state
data Status = Status
    { count :: !Int
      -- ^ Used to generate fresh unsolved variables (e.g. α̂, β̂ from the
      --   original paper)
    , context :: Context Location
      -- ^ The type-checking context (e.g. Γ, Δ, Θ)
    , stack :: Maybe (StackZipper (Doc AnsiStyle))
    }

data Binding
    = BExpr Text VariableId (Maybe Value)
    | forall a. BType VariableId (BindingDomain Location a) (Maybe a)

instance (MonadState Status m, MonadError TypeInferenceErrorS m) => ExistentialStore m [Binding] where
    unwrapEx domain ex = do
        ctx <- get
        -- FARENDE: EXISTENTIAL CUTOFF. E.g. ad data to store that limits resolvable existentials.
        -- I'm not sure if this is needed, but this *could* help ensure that
        -- existentials introduced earlier are not resolved through existentials introduced earlier.

        pure $ (\x -> ([], x)) <$> Context.unwrapEx domain ex ctx
instance (MonadState Status m, MonadError TypeInferenceErrorS m) => BindingsStore m [Binding] Location where
    bGet location (domain1 :: BindingDomain Location a) name1 = f where
        f :: [Binding] -> m (Maybe ([Binding], a))
        f store0 =
            case (domain1, store0) of
            -- TODO: Merge?
            (DomainType, []) -> throwErrorStack (UnboundTypeVariable location name1)
            (DomainFields, []) -> throwErrorStack (UnboundFields location name1)
            (DomainAlternatives, []) -> throwErrorStack (UnboundAlternatives location name1)
            (_, BType name2 domain2 val:xs) ->
                if name1 == name2
                    then case (domain1, domain2) of
                        (DomainType, DomainType) -> pure $ (xs,) <$> val
                        (DomainFields, DomainFields) -> pure $ (xs,) <$> val
                        (DomainAlternatives, DomainAlternatives) -> pure $ (xs,) <$> val
                        _domainMismatch -> f xs
                    else f xs
            (_, _:xs) -> f xs
    
    bSet domain name store val = pure (BType name domain (Just val):store)
    -- getFields location name = \case
    --     [] -> throwErrorStack (UnboundFields location name)
    --     (BFields name2 remaining:xs) | name == name2
    --         -> pure $ (\remaining' -> (xs, remaining')) <$> remaining
    --     (_:xs) -> Type.getFields location name xs
    -- getAlternatives location name = \case
    --     [] -> throwErrorStack (UnboundAlternatives location name)
    --     (BAlternatives name2 remaining:xs) | name == name2
    --         -> pure $ (\remaining' -> (xs, remaining')) <$> remaining
    --     (_:xs) -> Type.getAlternatives location name xs
    -- setType name store type_ = pure (BType name (Just type_):store)
    -- getType location name x = traceM (show x) *> traceM (show name) *> case x of
    --     [] -> throwErrorStack (UnboundTypeVariable location name)
    --     (BType name2 type_:xs) | name == name2
    --         -> pure $ RType xs <$> type_
    --     (_:xs) -> Type.getType location name xs
    -- setAlternatives?
    -- setFields?


type Type' = Type Location
type RType' = RType [Binding] Location

orDie :: (MonadState Status m, MonadError TypeInferenceErrorS m) => Maybe a -> TypeInferenceError -> m a
Just x  `orDie` _ = return x
Nothing `orDie` e = throwErrorStack e

freshInt :: MonadState Status m => m Int
freshInt = do
    Status{ count = n, .. } <- State.get
    State.put $! Status{ count = n + 1, .. }
    pure n

-- | Generate a fresh existential variable (of any type)
fresh :: MonadState Status m => m (Existential a)
fresh = UnsafeExistential <$> freshInt

_Context :: Lens' Status (Context Location)
_Context = lens context (\s context -> s { context })

_Entries :: Lens' Status [Entry]
_Entries = _Context . Context._Entries

-- Unlike the original paper, we don't explicitly thread the `Context` around.
-- Instead, we modify the ambient state using the following utility functions:

-- | Push a new `Context` `Entry` onto the stack
push :: MonadState Status m => Entry -> m ()
push entry = State.modify
    (\s -> s { context = (Context._Entries %~ (entry:)) (context s), stack = pushStack (pretty entry) <$> stack s })

-- | Retrieve the current `Context` `Entry`s
get :: MonadState Status m => m [Entry]
get = State.gets (view _Entries)

-- | Set the `Context` `Entry`s to a new value
set :: MonadState Status m => [Entry] -> m ()
set entries = State.modify (_Entries .~ entries)

scopedStack :: MonadState Status m => Doc AnsiStyle -> m a -> m a
scopedStack a k = do
    State.modify (\s -> s { stack = scopeStack (pretty a) <$> stack s })
    r <- k
    State.modify (\s -> s { stack = unscopeStack <$> stack s })
    return r

variableType :: MonadState Status m => Location -> Domain' -> m (Entry, Type.Binding Location)
variableType location = \case
    DomainType -> do
        existential <- fresh
        pure (Context.Variable DomainType existential, Type.Binding DomainType $ Type.VariableType { .. })
    DomainFields -> do
        existential <- fresh
        pure (Context.Variable DomainFields existential, Type.Binding DomainFields $ Type.Fields [] $ Right $ Monotype.VariableFields existential)
    DomainAlternatives -> do
        existential <- fresh
        pure (Context.Variable DomainAlternatives existential, Type.Binding DomainAlternatives $ Type.Alternatives [] $ Right $ Monotype.VariableAlternatives existential)


{-| This is used to temporarily add a `Context` `Entry` that is discarded at the
    end of the entry's scope, along with any downstream entries that were
    created within that same scope
-}
scoped :: MonadState Status m => Entry -> m r -> m r
scoped entry k = scopedStack "<scope>" do
    push entry

    r <- k

    State.modify (_Entries %~ Context.discardUpTo entry)

    return r

scopedVariableType :: MonadState Status m => Location -> Domain' -> VariableId -> (([Binding] -> [Binding]) -> m a) -> m a
scopedVariableType location domain oldId k = do
    (entry, Type.Binding domain2 value) <- variableType location domain
    scoped entry $ k \xs -> BType oldId domain2 (Just value) :xs

unsolvedType :: MonadState Status m => Location -> Domain' -> m (Entry, Entry, Type.Binding Location)
unsolvedType location domain = case domain of
    DomainType -> do
        x <- fresh
        pure (Context.Marker DomainType x, Context.Unsolved DomainType x, Type.Binding DomainType $ Type.UnsolvedType { existential = x, .. })
    DomainFields -> do
        x <- fresh
        pure (Context.Marker DomainFields x, Context.Unsolved DomainFields x, Type.Binding DomainFields $ Type.Fields [] $ Right $ Monotype.UnsolvedFields x)
    DomainAlternatives -> do
        x <- fresh
        pure (Context.Marker DomainAlternatives x, Context.Unsolved DomainAlternatives x, Type.Binding DomainAlternatives $ Type.Alternatives [] $ Right $ Monotype.UnsolvedAlternatives x)

scopedUnsolvedType :: MonadState Status m => Location -> Domain' -> VariableId -> (([Binding] -> [Binding]) -> m a) -> m a
scopedUnsolvedType location domain oldId k = do
    (marker, entry, Type.Binding domain2 value) <- unsolvedType location domain
    scoped marker do
        push entry

        k (\xs -> BType oldId domain2 (Just value):xs)

-- resolveUnresolveds :: (MonadState Status m, MonadError TypeInferenceErrorS m) => Type Location -> m (Type Location)
-- resolveUnresolveds = Type.substituteUnresolveds \location nameUnresolved parameters -> case nameUnresolved of
--     Left rawName -> do
--         _Γ <- get
--         (variableId, _, _) <- Context.lookupUnresolved (\(VariableId rawName' _) -> rawName == rawName') _Γ `orDie` UnboundUnresolved location rawName
--         return $ Type.Unresolved { nameUnresolved = Right variableId, .. }
--     Right _ -> return $ Type.Unresolved {..}

-- unwrapUnresolved :: (MonadState Status m, MonadError TypeInferenceErrorS m) => Location -> Either Text VariableId -> [Type s] -> [Entry s] -> m (Type s)
-- unwrapUnresolved = \location unresolvedName parameters _Γ -> do
--     variableId <- case unresolvedName of
--         Left n -> throwErrorStack $ MissingUnresolvedResolution location n
--         Right v -> pure v
--     (_, parameterNames, type_) <- Context.lookupUnresolved (== variableId) _Γ `orDie`
--         UnboundUnresolved location (either prettyToText prettyToText unresolvedName)
--     unwrap' parameterNames parameters type_
--   where
--     unwrap' [] [] type_ = return type_
--     unwrap' (name:names) (parameter:parameters) type_ =
--         unwrap' names parameters $ Type.substituteType name parameter type_
--     unwrap' _ _ _ = throwErrorStack UnresolvedMismatch
-- TODO: Not just Type, but also Fields and Alternatives...

{-| This corresponds to the judgment:

    > Γ ⊢ A

    … which checks that under context Γ, the type A is well-formed
-}
-- FARENDE: ?
-- wellFormedType :: Monad m => a -> b -> m ()
-- wellFormedType _ _ = pure ()
-- wellFormedType
--     :: (MonadState Status m, MonadError TypeInferenceErrorS m)
--     => [Entry] -> Type Location -> m ()
-- wellFormedType _Γ type0 =
--     case type0 of
--         -- UvarWF
--         Type.VariableType{..}
--             | Context.Variable DomainType name `elem` _Γ -> do
--                 return ()
--             | otherwise -> throwErrorStack (UnboundTypeVariable location name)

--         -- ArrowWF
--         Type.Function{..} -> do
--             wellFormedType _Γ input
--             wellFormedType _Γ output

--         -- ForallWF
--         Type.Forall{..} -> do
--             wellFormedType (Context.Variable domain name : _Γ) type_

--         -- ForallWF
--         Type.Exists{..} -> do
--             wellFormedType (Context.Variable domain name : _Γ) type_

--         -- EvarWF / SolvedEvarWF
--         _A@Type.UnsolvedType{..}
--             | any predicate _Γ -> do
--                 return ()
--             | otherwise -> do
--                 throwErrorStack (IllFormedType location _A _Γ)
--           where
--             predicate (Context.Unsolved DomainType a  ) = existential == a
--             predicate (Context.Solved DomainType   a _) = existential == a
--             predicate  _                         = False

--         -- Type.Unresolved {..} -> do
--         --     -- TODO: Simplify check?
--         --     type_ <- unwrapUnresolved location nameUnresolved parameters _Γ
--         --     wellFormedType _Γ type_

--         Type.Optional{..} -> do
--             wellFormedType _Γ type_

--         Type.List{..} -> do
--             wellFormedType _Γ type_

--         Type.Record{ fields = Type.Fields kAs (Right Monotype.EmptyFields) } -> do
--             traverse_ (\(_, _A) -> wellFormedType _Γ _A) kAs

--         Type.Record{ fields = Type.Fields kAs (Right (Monotype.UnsolvedFields a0)), .. }
--             | any predicate _Γ -> do
--                 traverse_ (\(_, _A) -> wellFormedType _Γ _A) kAs
--             | otherwise -> do
--                 throwErrorStack (IllFormedFields location a0 _Γ)
--           where
--             predicate (Context.Unsolved DomainFields a1  ) = a0 == a1
--             predicate (Context.Solved DomainFields   a1 _) = a0 == a1
--             predicate  _                            = False

--         Type.Record{ fields = Type.Fields kAs (Right (Monotype.VariableFields a)), .. }
--             | Context.Variable DomainFields a `elem` _Γ -> do
--                 traverse_ (\(_, _A) -> wellFormedType _Γ _A) kAs
--             | otherwise -> do
--                 throwErrorStack (UnboundFields location a)

--         Type.Union{ alternatives = Type.Alternatives kAs (Right Monotype.EmptyAlternatives) } -> do
--             traverse_ (\(_, _A) -> wellFormedType _Γ _A) kAs

--         Type.Union{ alternatives = Type.Alternatives kAs (Right (Monotype.UnsolvedAlternatives a0)), .. }
--             | any predicate _Γ -> do
--                 traverse_ (\(_, _A) -> wellFormedType _Γ _A) kAs
--             | otherwise -> do
--                 throwErrorStack (IllFormedAlternatives location a0 _Γ)
--           where
--             predicate (Context.Unsolved DomainAlternatives a1  ) = a0 == a1
--             predicate (Context.Solved DomainAlternatives   a1 _) = a0 == a1
--             predicate  _                                  = False

--         Type.Union{ alternatives = Type.Alternatives kAs (Right (Monotype.VariableAlternatives a)), .. }
--             | Context.Variable DomainAlternatives a `elem` _Γ -> do
--                 traverse_ (\(_, _A) -> wellFormedType _Γ _A) kAs
--             | otherwise -> do
--                 throwErrorStack (UnboundAlternatives location a)

--         Type.Scalar{} -> do
--             return ()

capturedType :: MonadState Status m => ([Binding], t Location) -> m (CapturedType t Location)
capturedType (ctx, type_) = do
    status <- State.get
    pure $ CapturedType (\x -> either (const Nothing) Just $ runExcept @TypeInferenceErrorS $ State.evalStateT @_ @Status x status) ctx type_

capturedRType :: MonadState Status m => RType [Binding] Location -> m (CapturedType Type Location)
capturedRType (RType ctx type_) = capturedType (ctx, type_)

scopedStackSubtype :: MonadState Status m => RType [Binding] Location -> RType [Binding] Location -> m b -> m b
scopedStackSubtype _A0 _B0 k = do
    _A <- capturedRType _A0
    _B <- capturedRType _B0
    scopedStack ("<subtype check> " <> pretty _A <> " <: " <> pretty _B) k

{-| This corresponds to the judgment:

    > Γ ⊢ A <: B ⊣ Δ

    … which updates the context Γ to produce the new context Δ, given that the
    type A is a subtype of type B.
-}
subtype
    :: (MonadState Status m, MonadError TypeInferenceErrorS m)
    => RType' -> RType' -> m ()
subtype _A0 _B0 = scopedStackSubtype _A0 _B0 do
    (_A0ctx, _A0type) <- Type.unwrapType _A0
    (_B0ctx, _B0type) <- Type.unwrapType _B0
    _Γ <- get
    -- traceM $ show _B0type

    case (_A0type, _B0type) of
        -- <:Var
        (Type.VariableType{ existential = a0 }, Type.VariableType{ existential = a1 })
            | a0 == a1 -> pure ()

        -- <:Exvar
        (Type.UnsolvedType{ existential = a0 }, Type.UnsolvedType{ existential = a1 })
            | a0 == a1 -> pure ()

        -- InstantiateL
        (Type.UnsolvedType{ existential = a }, _) -> instantiateTypeL a _B0
            -- The `not (a `Type.typeFreeIn` _B)` is the "occurs check" which
            -- prevents a type variable from being defined in terms of itself
            -- (i.e. a type should not "occur" within itself).
            --
            -- Later on you'll see matching "occurs checks" for record types and
            -- union types so that Fields variables and Alternatives variables
            -- cannot refer to the record or union that they belong to,
            -- respectively.
            -- |   not (a `Type.typeFreeIn` _B0)
            -- FARENDE: ?
            -- &&  elem (Context.Unsolved DomainType a) _Γ -> do
            --     instantiateTypeL a _B0

        -- InstantiateR
        (_, Type.UnsolvedType{ existential = a }) -> do
            traceM "Inst?"
            instantiateTypeR _A0 a <* traceM "Inst."
            -- |   not (a `Type.typeFreeIn` _A0)
            -- &&  elem (Context.Unsolved DomainType a) _Γ -> do
            -- FARENDE: ?
                -- instantiateTypeR _A0 a

        -- (Type.Unresolved{..}, _) -> do
        --     _Γ <- get
        --     type_ <- unwrapUnresolved location nameUnresolved parameters _Γ
        --     subtype type_ _B0

        -- (_, Type.Unresolved{..}) -> do
        --     _Γ <- get
        --     type_ <- unwrapUnresolved location nameUnresolved parameters _Γ
        --     subtype _A0 type_

        -- <:→
        (Type.Function{ input = _A1, output = _A2 }, Type.Function{ input= _B1, output = _B2 }) -> do
            subtype (RType _B0ctx _B1) (RType _A0ctx _A1)

            -- CAREFULLY NOTE: Pay really close attention to how we need to use
            -- `Context.solveType` any time we update the context.  The paper
            -- already mentions this, but if you forget to do this then you
            -- will get bugs due to unsolved variables not getting solved
            -- correctly.
            --
            -- A much more reliable way to fix this problem would simply be to
            -- have every function (like `subtype`, `instantiateL`, …)
            -- apply `solveType` to its inputs.  For example, this very
            -- `subtype` function could begin by doing:
            --
            --     _Γ <- get
            --     let _A0' = Context.solveType _Γ _A0
            --     let _B0' = Context.solveType _Γ _B0
            --
            -- … and then use _A0' and _B0' for downstream steps.  If we did
            -- that at the beginning of each function then everything would
            -- "just work".
            --
            -- However, this would be more inefficient because we'd calling
            -- `solveType` wastefully over and over with the exact same context
            -- in many cases.  So, the tradeoff here is that we get improved
            -- performance if we're willing to remember to call `solveType` in
            -- the right places.
            subtype (RType _A0ctx _A2) (RType _B0ctx _B2)

        -- One of the main extensions that is not present in the original paper
        -- is the addition of existential quantification.  This was actually
        -- pretty easy to implement: you just take the rules for universal
        -- quantification and flip them around and everything works.  Elegant!
        --
        -- For example, the <:∃R rule is basically the same as the <:∀L rule,
        -- except with the arguments flipped.  Similarly, the <:∃L rule is
        -- basically the same as the <:∀R rule with the arguments flipped.

        -- <:∃L
        (Type.Exists{..}, _) -> do
            scopedVariableType nameLocation domain name \upd ->
                subtype (RType (upd _A0ctx) type_) _B0
            -- scopedVariableType nameLocation domain name \_ substitute ->
            --     subtype (substitute type_) _B0

        -- <:∀R
        (_, Type.Forall{..}) -> do
            scopedVariableType nameLocation domain name \upd ->
                subtype _A0 (RType (upd _B0ctx) type_)
            -- scopedVariableType nameLocation domain name \_ substitute->
            --     subtype _A0 (substitute type_)

        -- <:∃R
        (_, Type.Exists{ .. }) -> do
            scopedUnsolvedType nameLocation domain name \upd ->
                subtype _A0 (RType (upd _B0ctx) type_)
            -- scopedUnsolvedType nameLocation \a ->
            --     subtype _A0 (Type.substituteType name a type_)

        -- <:∀L
        (Type.Forall{ .. }, _) -> do
            scopedUnsolvedType nameLocation domain name \upd -> do
                subtype (RType (upd _A0ctx) type_) _B0
            -- scopedUnsolvedType nameLocation \a -> do
            --     subtype (Type.substituteType name a type_) _B0

        (Type.Scalar{ scalar = s0 }, Type.Scalar{ scalar = s1 })
            | s0 == s1 -> do
                return ()

        (Type.Optional{ type_ = _A }, Type.Optional{ type_ = _B }) -> do
            subtype (RType _A0ctx _A) (RType _B0ctx _B)

        (Type.List{ type_ = _A }, Type.List{ type_ = _B }) -> do
            subtype (RType _A0ctx _A) (RType _B0ctx _B)

        -- This is where you need to add any non-trivial subtypes.  For example,
        -- the following three rules specify that `Natural` is a subtype of
        -- `Integer`, which is in turn a subtype of `Real`.
        (Type.Scalar{ scalar = Monotype.Natural }, Type.Scalar{ scalar = Monotype.Integer }) -> do
            return ()

        (Type.Scalar{ scalar = Monotype.Natural }, Type.Scalar{ scalar = Monotype.Real }) -> do
            return ()

        (Type.Scalar{ scalar = Monotype.Integer }, Type.Scalar{ scalar = Monotype.Real }) -> do
            return ()

        -- Similarly, this is the rule that says that `T` is a subtype of
        -- `Optional T`.  If that feels unprincipled to you then delete this
        -- rule.
        (_, Type.Optional{..}) -> do
            subtype _A0 (RType _B0ctx type_)

        (Type.Scalar{ }, Type.Scalar{ scalar = Monotype.JSON }) -> do
            return ()

        (Type.List{ type_ = _A }, Type.Scalar{ scalar = Monotype.JSON }) -> do
            subtype (RType _A0ctx _A) _B0
            return ()

        (Type.Record{ fields = Type.Fields kAs (Right Monotype.EmptyFields) }, Type.Scalar{ scalar = Monotype.JSON }) -> do
            let process (_, _A) = subtype (RType _A0ctx _A) _B0

            traverse_ process kAs

        -- The type-checking code for records is the first place where we
        -- implement a non-trivial type that wasn't already covered by the
        -- paper, so we'll go into more detail here to explain the general
        -- type-checking principles of the paper.
        (Type.Record{ fields = Type.Fields kAs0 (Right fields0) }, Type.Record{ fields = Type.Fields kBs0 (Right fields1) }) -> do
            let mapA = Map.fromList kAs0
            let mapB = Map.fromList kBs0

            let extraA = Map.difference mapA mapB
            let extraB = Map.difference mapB mapA

            let both = Map.intersectionWith (,) mapA mapB

            let flexible  Monotype.EmptyFields          = False
                flexible (Monotype.VariableFields _   ) = False
                flexible (Monotype.UnsolvedFields _   ) = True

            let okayA = Map.null extraA
                    || (flexible fields1 && fields0 /= fields1)

            let okayB = Map.null extraB
                    || (flexible fields0 && fields0 /= fields1)

            -- First we check that there are no mismatches in the record types
            -- that cannot be resolved by just setting an unsolved Fields
            -- variable to the right type.
            --
            -- For example, `{ x: Bool }` can never be a subtype of
            -- `{ y: Text }`
            _A0c <- capturedRType _A0
            _B0c <- capturedRType _B0
            extraAc <- traverse (capturedRType . RType _A0ctx) extraA
            extraBc <- traverse (capturedRType . RType _B0ctx) extraB
            if | not okayA && not okayB -> do
                throwErrorStack (RecordTypeMismatch _A0c _B0c extraAc extraBc)

               | not okayA -> do
                throwErrorStack (RecordTypeMismatch _A0c _B0c extraAc mempty)

               | not okayB -> do
                throwErrorStack (RecordTypeMismatch _A0c _B0c mempty extraBc)

               | otherwise -> do
                   return ()

            -- If record A is a subtype of record B, then all fields in A
            -- must be a subtype of the matching fields in record B
            let process (_A1, _B1) = subtype
                        (RType _A0ctx _A1)
                        (RType _B0ctx _B1)

            -- We only check fields are present in `both` records.  For
            -- mismatched fields present only in one record type we have to
            -- skip to the next step of resolving the mismatch by solving Fields
            -- variables.
            traverse_ process both

            -- Here is where we handle fields that were only present in one
            -- record type.  They still might be okay if one or both of the
            -- record types has an unsolved fields variable.
            case (fields0, fields1) of
                -- The two records are identical, so there's nothing left to do
                _ | null extraA && null extraB && fields0 == fields1 -> do
                        return ()

                -- Both records type have unsolved Fields variables.  Great!
                -- This is the most flexible case, since we can replace these
                -- unsolved variables with whatever fields we want to make the
                -- types match.
                --
                -- However, it's not as simple as setting each Fields variable
                -- to the extra fields from the opposing record type.  For
                -- example, if the two record types we're comparing are:
                --
                --     { x: Bool, p0 } <: { y: Text, p1 }
                --
                -- … then it's not correct to say:
                --
                --     p0 = y: Text
                --     p1 = x: Bool
                --
                -- … because that is not the most general solution for `p0` and
                -- `p1`!  The actual most general solution is:
                --
                --     p0 = { y: Text, p2 }
                --     p1 = { x: Bool, p2 }
                --
                -- … where `p2` is a fresh Fields type variable representing the
                -- fact that both records could potentially have even more
                -- fields other than `x` and `y`.
                (Monotype.UnsolvedFields p0, Monotype.UnsolvedFields p1) -> do
                    p2 <- fresh

                    _Γ0 <- get

                    -- We have to insert p2 before both p0 and p1 within the
                    -- context because the bidirectional type-checking algorithm
                    -- requires that the context is ordered and all variables
                    -- within the context can only reference prior variables
                    -- within the context.
                    --
                    -- Since `p0` and `p1` both have to reference `p2`, then we
                    -- need to insert `p2` right before `p0` or `p1`, whichever
                    -- one comes first
                    let p0First = do
                            (_ΓR, _ΓL) <- Context.splitOnUnsolved DomainFields p0 _Γ0

                            Monad.guard (Context.Unsolved DomainFields p1 `elem` _ΓR)

                            let command =
                                    set (   _ΓR
                                        <>  ( Context.Unsolved DomainFields p0
                                            : Context.Unsolved DomainFields p2
                                            : _ΓL
                                            )
                                        )

                            return command

                    let p1First = do
                            (_ΓR, _ΓL) <- Context.splitOnUnsolved DomainFields p1 _Γ0

                            Monad.guard (Context.Unsolved DomainFields p0 `elem` _ΓR)

                            let command =
                                    set (   _ΓR
                                        <>  ( Context.Unsolved DomainFields p1
                                            : Context.Unsolved DomainFields p2
                                            : _ΓL
                                            )
                                        )

                            return command

                    case p0First <|> p1First of
                        Nothing -> do
                            throwErrorStack (MissingOneOfFields [Type.location _A0type, Type.location _B0type] p0 p1 _Γ)

                        Just setContext -> do
                            setContext

                    _Θ <- get

                    -- Now we solve for `p0`.  This is basically saying:
                    --
                    -- p0 = { extraFieldsFromRecordB, p2 }
                    instantiateFieldsL
                        _B0ctx
                        p0
                        (Type.location _B0type)
                        (Type.Fields (Map.toList extraB) (Right $ Monotype.UnsolvedFields p2))

                    _Δ <- get

                    -- Similarly, solve for `p1`.  This is basically saying:
                    --
                    -- p1 = { extraFieldsFromRecordA, p2 }
                    instantiateFieldsR
                        _A0ctx
                        (Type.location _A0type)
                        (Type.Fields (Map.toList extraA) (Right $ Monotype.UnsolvedFields p2))
                        p1

                -- If only one of the records has a Fields variable then the
                -- solution is simpler: just set the Fields variable to the
                -- extra fields from the opposing record
                (Monotype.UnsolvedFields p0, _) ->
                    instantiateFieldsL
                        _B0ctx
                        p0
                        (Type.location _B0type)
                        (Type.Fields (Map.toList extraB) (Right fields1))

                (_, Monotype.UnsolvedFields p1) -> do
                    _Θ <- get

                    instantiateFieldsR
                        _A0ctx
                        (Type.location _A0type)
                        (Type.Fields (Map.toList extraA) (Right fields0))
                        p1

                (_, _) -> do
                    throwErrorStack (NotRecordSubtype (Type.location _A0type) _A0c (Type.location _B0type) _B0c)

        -- Checking if one union is a subtype of another union is basically the
        -- exact same as the logic for checking if a record is a subtype of
        -- another record.
        (Type.Union{ alternatives = Type.Alternatives kAs0 (Right alternatives0) }, Type.Union{ alternatives = Type.Alternatives kBs0 (Right alternatives1) }) -> do
            let mapA = Map.fromList kAs0
            let mapB = Map.fromList kBs0

            let extraA = Map.difference mapA mapB
            let extraB = Map.difference mapB mapA

            let both = Map.intersectionWith (,) mapA mapB

            let flexible  Monotype.EmptyAlternatives          = False
                flexible (Monotype.VariableAlternatives _   ) = False
                flexible (Monotype.UnsolvedAlternatives _   ) = True

            let okayA = Map.null extraA
                    ||  (flexible alternatives1 && alternatives0 /= alternatives1)
            let okayB = Map.null extraB
                    ||  (flexible alternatives0 && alternatives0 /= alternatives1)

            _A0c <- capturedRType _A0
            _B0c <- capturedRType _B0
            extraAc <- traverse (capturedRType . RType _A0ctx) extraA
            extraBc <- traverse (capturedRType . RType _B0ctx) extraB
            if | not okayA && not okayB -> do
                throwErrorStack (UnionTypeMismatch _A0c _B0c extraAc extraBc)

               | not okayA && okayB -> do
                throwErrorStack (UnionTypeMismatch _A0c _B0c extraAc mempty)

               | okayA && not okayB -> do
                throwErrorStack (UnionTypeMismatch _A0c _B0c mempty extraBc)

               | otherwise -> do
                return ()

            let process (_A1, _B1) = subtype (RType _A0ctx _A1) (RType _B0ctx _B1)

            traverse_ process both

            case (alternatives0, alternatives1) of
                _ | null extraA && null extraB && alternatives0 == alternatives1 -> do
                        return ()

                (Monotype.UnsolvedAlternatives p0, Monotype.UnsolvedAlternatives p1) -> do
                    p2 <- fresh

                    _Γ0 <- get

                    let p0First = do
                            (_ΓR, _ΓL) <- Context.splitOnUnsolved DomainAlternatives p0 _Γ0

                            Monad.guard (Context.Unsolved DomainAlternatives p1 `elem` _ΓR)

                            let command =
                                    set (   _ΓR
                                        <>  ( Context.Unsolved DomainAlternatives p0
                                            : Context.Unsolved DomainAlternatives p2
                                            : _ΓL
                                            )
                                        )

                            return command

                    let p1First = do
                            (_ΓR, _ΓL) <- Context.splitOnUnsolved DomainAlternatives p1 _Γ0

                            Monad.guard (Context.Unsolved DomainAlternatives p0 `elem` _ΓR)

                            let command =
                                    set (   _ΓR
                                        <>  ( Context.Unsolved DomainAlternatives p1
                                            : Context.Unsolved DomainAlternatives p2
                                            : _ΓL
                                            )
                                        )

                            return command

                    case p0First <|> p1First of
                        Nothing -> do
                            throwErrorStack (MissingOneOfAlternatives [Type.location _A0type, Type.location _B0type] p0 p1 _Γ)

                        Just setContext -> do
                            setContext

                    _Θ <- get

                    instantiateAlternativesL
                        _B0ctx
                        p0
                        (Type.location _B0type)
                        (Type.Alternatives (Map.toList extraB) (Right $ Monotype.UnsolvedAlternatives p2))

                    _Δ <- get

                    instantiateAlternativesR
                        _A0ctx
                        (Type.location _A0type)
                        (Type.Alternatives (Map.toList extraA) (Right $ Monotype.UnsolvedAlternatives p2))
                        p1

                (Monotype.EmptyAlternatives, Monotype.EmptyAlternatives) -> do
                    return ()

                (Monotype.UnsolvedAlternatives p0, _) -> do
                    _Θ <- get

                    instantiateAlternativesL
                        _B0ctx
                        p0
                        (Type.location _B0type)
                        (Type.Alternatives (Map.toList extraB) (Right alternatives1))

                (Monotype.VariableAlternatives p0, Monotype.VariableAlternatives p1)
                    | p0 == p1 -> do
                        return ()

                (_, Monotype.UnsolvedAlternatives p1) -> do
                    _Θ <- get

                    instantiateAlternativesR
                        _A0ctx
                        (Type.location _A0type)
                        (Type.Alternatives (Map.toList extraA) (Right alternatives0))
                        p1

                (_, _) -> do
                    throwErrorStack (NotUnionSubtype (Type.location _A0type) _A0c (Type.location _B0type) _B0c)

        -- Unfortunately, we need to have this wildcard match at the end,
        -- otherwise we'd have to specify a number of cases that is quadratic
        -- in the number of `Type` constructors.  That in turn means that you
        -- can easily forget to add cases like:
        --
        --     (Type.List _A, Type.List _B) -> do
        --         subtype _A _B
        --
        -- … because the exhaustivity checker won't warn you if you forget to
        -- add that case.
        --
        -- The way I remember to do this is that when I add new complex types I
        -- grep the codebase for all occurrences of an existing complex type
        -- (like `List`), and then one of the occurrences will be here in this
        -- `subtype` function and then I'll remember to add a case for my new
        -- complex type here.
        (_, _) -> do
            traceM "hi"
            traceM (show _A0type)
            traceM (show _B0type)

            _A0c <- capturedRType _A0
            _B0c <- capturedRType _B0
            throwErrorStack (NotSubtype (Type.location _A0type) _A0c (Type.location _B0type) _B0c)

{-| This corresponds to the judgment:

    > Γ ⊢ α̂ :≦ A ⊣ Δ

    … which updates the context Γ to produce the new context Δ, by instantiating
    α̂ such that α̂ <: A.

    The @instantiate*@ family of functions should really be called @solve*@
    because their job is to solve an unsolved variable within the context.
    However, for consistency with the paper we still name them @instantiate*@.
-}
instantiateTypeL
    :: (MonadState Status m, MonadError TypeInferenceErrorS m)
    => Existential Monotype -> RType' -> m ()
instantiateTypeL a _A0 = do
    _Γ0 <- get

    (_Γ', _Γ) <- Context.splitOnUnsolved DomainType a _Γ0 `orDie` MissingVariable a _Γ0

    let instLSolve τ =
            -- wellFormedType _Γ _A0

            set (_Γ' <> (Context.Solved DomainType a τ : _Γ))

    (_A0ctx, _A0type) <- Type.unwrapType _A0

    case _A0type of
        -- Type.Unresolved{..} -> do
        --     let _ΓL = _Γ
        --     type_ <- unwrapUnresolved location nameUnresolved parameters _ΓL
        --     instantiateTypeL a type_

        Type.Unresolved{} -> pure () -- FARENDE: ?
        Type.Assign{} -> pure ()

        -- InstLReach
        Type.UnsolvedType{..}
            | let _ΓL = _Γ
            , Just (_ΓR, _ΓM) <- Context.splitOnUnsolved DomainType existential _Γ' -> do
                set (_ΓR <> (Context.Solved DomainType existential (Monotype.UnsolvedType a) : _ΓM) <> (Context.Unsolved DomainType a : _ΓL))

        -- InstLSolve
        Type.UnsolvedType{..} -> do
            instLSolve (Monotype.UnsolvedType existential)
        Type.VariableType{..} -> do
            instLSolve (Monotype.VariableType existential)
        Type.Scalar{..} -> do
            instLSolve (Monotype.Scalar scalar)

        -- InstLExt
        Type.Exists{..} ->
            scopedUnsolvedType nameLocation domain name \upd ->
                instantiateTypeR (RType (upd _A0ctx) _A0type) a
        --     scopedUnsolvedType nameLocation \b -> do
        --         instantiateTypeR (Type.substituteType name b type_) a
        -- Type.Exists{ domain = DomainFields, .. } -> do
        --     scopedUnsolvedFields \b -> do
        --         instantiateTypeR (Type.substituteFields name b type_) a
        -- Type.Exists{ domain = DomainAlternatives, .. } -> do
        --     scopedUnsolvedAlternatives \b -> do
        --         instantiateTypeR (Type.substituteAlternatives name b type_) a

        -- InstLArr
        Type.Function{..} -> do
            let _ΓL = _Γ
            let _ΓR = _Γ'

            a1 <- fresh
            a2 <- fresh

            set (_ΓR <> (Context.Solved DomainType a (Monotype.Function (Monotype.UnsolvedType a1) (Monotype.UnsolvedType a2)) : Context.Unsolved DomainType a1 : Context.Unsolved DomainType a2 : _ΓL))

            instantiateTypeR (RType _A0ctx input) a1
            instantiateTypeL a2 (RType _A0ctx output)

        -- InstLAllR
        Type.Forall{..} ->
            scopedVariableType nameLocation domain name \upd ->
                instantiateTypeL a (RType (upd _A0ctx) type_)

            -- scopedVariableType nameLocation domain name \_ substitute ->
            --     instantiateTypeL a (substitute type_)

        -- This case is the first example of a general pattern we have to
        -- follow when solving unsolved variables.
        --
        -- Typically when you solve an unsolved variable (e.g. `a`) to some
        -- type (e.g. `A`), you cannot just directly solve the variable as:
        --
        --     a = A
        --
        -- … because unsolved variables can only be solved to `Monotype`s, but
        -- `A` is typically a `Type`.
        --
        -- So, instead, what you do is you solve the variable one layer at a
        -- time.  For example, if you try to solve `a` to (the `Type`)
        -- `Optional (List Bool)`, you will actually get three solved variables
        -- added to the context:
        --
        --     a = Optional b
        --     b = List c
        --     c = Bool
        --
        -- In other words, each time you solve one layer of a complex type, you
        -- need to create a fresh unsolved variable for each inner type and
        -- solve each inner unsolved variable.
        --
        -- This may seem really indirect and tedious, but if you try to skip
        -- this one-layer-at-a-time solving process then you will likely get
        -- bugs due to solved variables referring to each other out of order.
        --
        -- This wasn't obvious to me from reading the original paper since they
        -- didn't really cover how to type-check complex types other than
        -- function types.
        Type.Optional{..} -> do
            let _ΓL = _Γ
            let _ΓR = _Γ'

            -- To solve `a` against `Optional _A` we create a fresh unsolved
            -- variable named `a1`, …
            a1 <- fresh

            -- … solve `a` to `Optional a1`, taking care that `a1` comes before
            -- `a` within the context, (since `a` refers to `a1`)  …
            set (_ΓR <> (Context.Solved DomainType a (Monotype.Optional (Monotype.UnsolvedType a1)) : Context.Unsolved DomainType a1 : _ΓL))

            -- … and then solve `a1` against _A`
            instantiateTypeL a1 (RType _A0ctx type_)

        -- We solve an unsolved variable against `List` using the same
        -- principles described above for solving `Optional`
        Type.List{..} -> do
            let _ΓL = _Γ
            let _ΓR = _Γ'

            a1 <- fresh

            set (_ΓR <> (Context.Solved DomainType a (Monotype.List (Monotype.UnsolvedType a1)) : Context.Unsolved DomainType a1 : _ΓL))

            instantiateTypeL a1 (RType _A0ctx type_)

        -- This is still the same one-layer-at-a-time principle, with a small
        -- twist.  In order to solve:
        --
        --     a = { r }
        --
        -- We replace `r` with a new unsolved Fields variable and then solve for
        -- that Fields variable.
        Type.Record{..} -> do
            let _ΓL = _Γ
            let _ΓR = _Γ'

            p <- fresh

            set (_ΓR <> (Context.Solved DomainType a (Monotype.Record (Monotype.Fields [] (Monotype.UnsolvedFields p))) : Context.Unsolved DomainFields p : _ΓL))

            instantiateFieldsL _A0ctx p (Type.location _A0type) fields

        -- Same principle as for `Record`, but replacing the Field variable with
        -- an Alternatives variable
        Type.Union{..} -> do
            let _ΓL = _Γ
            let _ΓR = _Γ'

            p <- fresh

            set (_ΓR <> (Context.Solved DomainType a (Monotype.Union (Monotype.Alternatives [] (Monotype.UnsolvedAlternatives p))) : Context.Unsolved DomainAlternatives p : _ΓL))

            instantiateAlternativesL _A0ctx p (Type.location _A0type) alternatives

{-| This corresponds to the judgment:

    > Γ ⊢ A ≦: α̂ ⊣ Δ

    … which updates the context Γ to produce the new context Δ, by instantiating
    α̂ such that A :< α̂.
-}
instantiateTypeR
    :: (MonadState Status m, MonadError TypeInferenceErrorS m)
    => RType' -> Existential Monotype -> m ()
instantiateTypeR _A0 a = do
    _Γ0 <- get

    (_Γ', _Γ) <- Context.splitOnUnsolved DomainType a _Γ0 `orDie` MissingVariable a _Γ0

    let instRSolve τ =
            -- wellFormedType _Γ _A0

            set (_Γ' <> (Context.Solved DomainType a τ : _Γ))

    (_A0ctx, _A0type) <- Type.unwrapType _A0

    case _A0type of
        Type.Assign{} -> pure ()
        Type.Unresolved{} -> pure ()
            -- FARENDE: ?

        -- InstRReach
        Type.UnsolvedType{..}
            | let _ΓL = _Γ
            , Just (_ΓR, _ΓM) <- Context.splitOnUnsolved DomainType existential _Γ' -> do
                set (_ΓR <> (Context.Solved DomainType existential (Monotype.UnsolvedType a) : _ΓM) <> (Context.Unsolved DomainType a : _ΓL))

        -- InstRSolve
        Type.UnsolvedType{..} -> do
            instRSolve (Monotype.UnsolvedType existential)
        Type.VariableType{..} -> do
            instRSolve (Monotype.VariableType existential)
        Type.Scalar{..} -> do
            instRSolve (Monotype.Scalar scalar)

        -- InstRArr
        Type.Function{..} -> do
            let _ΓL = _Γ
            let _ΓR = _Γ'

            a1 <- fresh
            a2 <- fresh

            set (_ΓR <> (Context.Solved DomainType a (Monotype.Function (Monotype.UnsolvedType a1) (Monotype.UnsolvedType a2)) : Context.Unsolved DomainType a1 : Context.Unsolved DomainType a2 : _ΓL))

            instantiateTypeL a1 (RType _A0ctx input)
            instantiateTypeR (RType _A0ctx output) a2

        -- InstRExtL
        Type.Exists{..} -> do
            scopedVariableType nameLocation domain name \upd ->
                instantiateTypeL a (RType (upd _A0ctx) type_)
        -- Type.Exists{..} -> do
        --     scopedVariableType nameLocation domain name \_ substitute ->
        --         instantiateTypeL a (substitute type_)

        -- -- InstRAllL
        Type.Forall{..} -> do
            scopedUnsolvedType nameLocation domain name \upd ->
                instantiateTypeR (RType (upd _A0ctx) type_) a
        -- Type.Forall{ domain = DomainType, .. } -> do
        --     scopedUnsolvedType nameLocation \b -> do
        --         instantiateTypeR (Type.substituteType name b type_) a
        -- Type.Forall{ domain = DomainFields, .. } -> do
        --     scopedUnsolvedFields \b -> do
        --         instantiateTypeR (Type.substituteFields name b type_) a
        -- Type.Forall{ domain = DomainAlternatives, .. } -> do
        --     scopedUnsolvedAlternatives \b -> do
        --         instantiateTypeR (Type.substituteAlternatives name b type_) a

        Type.Optional{..} -> do
            let _ΓL = _Γ
            let _ΓR = _Γ'

            a1 <- fresh

            set (_ΓR <> (Context.Solved DomainType a (Monotype.Optional (Monotype.UnsolvedType a1)) : Context.Unsolved DomainType a1 : _ΓL))

            instantiateTypeR (RType _A0ctx type_) a1

        Type.List{..} -> do
            let _ΓL = _Γ
            let _ΓR = _Γ'

            a1 <- fresh

            set (_ΓR <> (Context.Solved DomainType a (Monotype.List (Monotype.UnsolvedType a1)) : Context.Unsolved DomainType a1 : _ΓL))

            instantiateTypeR (RType _A0ctx type_) a1

        Type.Record{..}  -> do
            let _ΓL = _Γ
            let _ΓR = _Γ'

            p <- fresh

            set (_ΓR <> (Context.Solved DomainType a (Monotype.Record (Monotype.Fields [] (Monotype.UnsolvedFields p))) : Context.Unsolved DomainFields p : _ΓL))

            instantiateFieldsR _A0ctx (Type.location _A0type) fields p

        Type.Union{..} -> do
            let _ΓL = _Γ
            let _ΓR = _Γ'

            p <- fresh

            set (_ΓR <> (Context.Solved DomainType a (Monotype.Union (Monotype.Alternatives [] (Monotype.UnsolvedAlternatives p))) : Context.Unsolved DomainAlternatives p : _ΓL))

            instantiateAlternativesR _A0ctx (Type.location _A0type) alternatives p

{- The following `equateFields` / `instantiateFieldsL` / `instantiateFieldsR`,
   `equateAlternatives` / `instantiateAlternativesL` /
   `instantiateAlternativesR` judgments are not present in the bidirectional
   type-checking paper.  These were added in order to support row polymorphism
   and variant polymorphism, by following the same general type-checking
   principles as the original paper.

   If you understand how the `instantiateTypeL` and `instantiateTypeR` functions
   work, then you will probably understand how these functions work because they
   follow the same rules:

   * Always make sure that solved variables only reference variables earlier
     within the context

   * Solve for unsolved variables one layer at a time

   Note that the implementation and the user-facing terminology use the term
   fields/alternatives instead of rows/variants, respectively.
-}

equateFields
    :: (MonadState Status m, MonadError TypeInferenceErrorS m)
    => Existential Monotype.Record -> Existential Monotype.Record -> m ()
equateFields p0 p1 = do
    _Γ0 <- get

    let p0First = do
            (_ΓR, _ΓL) <- Context.splitOnUnsolved DomainFields p1 _Γ0

            Monad.guard (Context.Unsolved DomainFields p0 `elem` _ΓL)

            return (set (_ΓR <> (Context.Solved DomainFields p1 (Monotype.Fields [] (Monotype.UnsolvedFields p0)) : _ΓL)))

    let p1First = do
            (_ΓR, _ΓL) <- Context.splitOnUnsolved DomainFields p0 _Γ0

            Monad.guard (Context.Unsolved DomainFields p1 `elem` _ΓL)

            return (set (_ΓR <> (Context.Solved DomainFields p0 (Monotype.Fields [] (Monotype.UnsolvedFields p1)) : _ΓL)))

    case p0First <|> p1First of
        Nothing -> do
            throwErrorStack (MissingOneOfFields [] p0 p1 _Γ0)

        Just setContext -> do
            setContext

instantiateFieldsL
    :: (MonadState Status m, MonadError TypeInferenceErrorS m)
    => [Binding]
    -> Existential Monotype.Record
    -> Location
    -> Type.Record Location
    -> m ()
instantiateFieldsL _A0ctx p0 location fields@(Type.Fields kAs rest) = do
    -- when (p0 `Type.fieldsFreeIn` Type.Record{..}) do
    --     throwErrorStack (NotFieldsSubtype location p0 fields)
    -- FARENDE: ?

    let process (k, _A) = do
            b <- fresh

            return (k, _A, b)

    kAbs <- traverse process kAs

    let bs  = map (\(_, _, b) -> Context.Unsolved DomainType b      ) kAbs
    let kbs = map (\(k, _, b) -> (k, Monotype.UnsolvedType b)) kAbs

    _Γ <- get

    (_ΓR, _ΓL) <- Context.splitOnUnsolved DomainFields p0 _Γ `orDie` MissingAllFields p0 _Γ

    case rest of
        Left _ -> pure () -- FARENDE: ?
        Right (Monotype.UnsolvedFields p1) -> do
            p2 <- fresh

            set (_ΓR <> (Context.Solved DomainFields p0 (Monotype.Fields kbs (Monotype.UnsolvedFields p2)) : Context.Unsolved DomainFields p2 : bs <> _ΓL))

            equateFields p1 p2

        Right rest' -> do
            -- FARENDE: ?

            set (_ΓR <> (Context.Solved DomainFields p0 (Monotype.Fields kbs rest') : bs <> _ΓL))

    let instantiate (_, _A, b) = instantiateTypeL b (RType _A0ctx _A)

    traverse_ instantiate kAbs

instantiateFieldsR
    :: (MonadState Status m, MonadError TypeInferenceErrorS m)
    => [Binding]
    -> Location
    -> Type.Record Location
    -> Existential Monotype.Record
    -> m ()
instantiateFieldsR _A0ctx location fields@(Type.Fields kAs rest) p0 = do
    -- when (p0 `Type.fieldsFreeIn` Type.Record{..}) do
    --     throwErrorStack (NotFieldsSubtype location p0 fields)
    -- FARENDE: ?

    let process (k, _A) = do
            b <- fresh

            return (k, _A, b)

    kAbs <- traverse process kAs

    let bs  = map (\(_, _, b) -> Context.Unsolved DomainType b      ) kAbs
    let kbs = map (\(k, _, b) -> (k, Monotype.UnsolvedType b)) kAbs

    _Γ <- get

    (_ΓR, _ΓL) <- Context.splitOnUnsolved DomainFields p0 _Γ `orDie` MissingAllFields p0 _Γ

    case rest of
        Left _ -> pure () -- FARENDE: ?
        Right (Monotype.UnsolvedFields p1) -> do
            p2 <- fresh

            set (_ΓR <> (Context.Solved DomainFields p0 (Monotype.Fields kbs (Monotype.UnsolvedFields p2)) : Context.Unsolved DomainFields p2 : bs <> _ΓL))

            equateFields p1 p2

        Right rest' -> do
            -- FARENDE: ?
            -- wellFormedType (bs <> _ΓL)
            --     Type.Record{ fields = Type.Fields [] rest, .. }

            set (_ΓR <> (Context.Solved DomainFields p0 (Monotype.Fields kbs rest') : bs <> _ΓL))

    let instantiate (_, _A, b) = instantiateTypeR (RType _A0ctx _A) b

    traverse_ instantiate kAbs

equateAlternatives
    :: (MonadState Status m, MonadError TypeInferenceErrorS m)
    => Existential Monotype.Union-> Existential Monotype.Union -> m ()
equateAlternatives p0 p1 = do
    _Γ0 <- get

    let p0First = do
            (_ΓR, _ΓL) <- Context.splitOnUnsolved DomainAlternatives p1 _Γ0

            Monad.guard (Context.Unsolved DomainAlternatives p0 `elem` _ΓL)

            return (set (_ΓR <> (Context.Solved DomainAlternatives p1 (Monotype.Alternatives [] (Monotype.UnsolvedAlternatives p0)) : _ΓL)))

    let p1First = do
            (_ΓR, _ΓL) <- Context.splitOnUnsolved DomainAlternatives p0 _Γ0

            Monad.guard (Context.Unsolved DomainAlternatives p1 `elem` _ΓL)

            return (set (_ΓR <> (Context.Solved DomainAlternatives p0 (Monotype.Alternatives [] (Monotype.UnsolvedAlternatives p1)) : _ΓL)))

    case p0First <|> p1First of
        Nothing -> do
            throwErrorStack (MissingOneOfAlternatives [] p0 p1 _Γ0)

        Just setContext -> do
            setContext

instantiateAlternativesL
    :: (MonadState Status m, MonadError TypeInferenceErrorS m)
    => [Binding]
    -> Existential Monotype.Union
    -> Location
    -> Type.Union Location
    -> m ()
instantiateAlternativesL _A0ctx p0 location alternatives@(Type.Alternatives kAs rest) = do
    -- when (p0 `Type.alternativesFreeIn` Type.Union{..}) do
    --     throwErrorStack (NotAlternativesSubtype location p0 alternatives)
    -- FARENDE: ?

    let process (k, _A) = do
            b <- fresh

            return (k, _A, b)

    kAbs <- traverse process kAs

    let bs  = map (\(_, _, b) -> Context.Unsolved DomainType b      ) kAbs
    let kbs = map (\(k, _, b) -> (k, Monotype.UnsolvedType b)) kAbs

    _Γ <- get

    (_ΓR, _ΓL) <- Context.splitOnUnsolved DomainAlternatives p0 _Γ `orDie` MissingAllAlternatives p0 _Γ

    case rest of
        Left _ -> pure () -- FARENDE: ?
        Right (Monotype.UnsolvedAlternatives p1) -> do
            p2 <- fresh

            set (_ΓR <> (Context.Solved DomainAlternatives p0 (Monotype.Alternatives kbs (Monotype.UnsolvedAlternatives p2)) : Context.Unsolved DomainAlternatives p2 : bs <> _ΓL))

            equateAlternatives p1 p2

        Right rest' -> do
            -- FARENDE: ?
            -- wellFormedType (bs <> _ΓL)
            --     Type.Union{ alternatives = Type.Alternatives [] rest, .. }

            set (_ΓR <> (Context.Solved DomainAlternatives p0 (Monotype.Alternatives kbs rest') : bs <> _ΓL))

    let instantiate (_, _A, b) = do
            _Θ <- get

            instantiateTypeL b (RType _A0ctx _A)

    traverse_ instantiate kAbs

instantiateAlternativesR
    :: (MonadState Status m, MonadError TypeInferenceErrorS m)
    => [Binding]
    -> Location
    -> Type.Union Location
    -> Existential Monotype.Union
    -> m ()
instantiateAlternativesR _A0ctx location alternatives@(Type.Alternatives kAs rest) p0 = do
    -- when (p0 `Type.alternativesFreeIn` Type.Union{..}) do
    --     throwErrorStack (NotAlternativesSubtype location p0 alternatives)
    -- FARENDE: ?

    let process (k, _A) = do
            b <- fresh

            return (k, _A, b)

    kAbs <- traverse process kAs

    let bs  = map (\(_, _, b) -> Context.Unsolved DomainType b      ) kAbs
    let kbs = map (\(k, _, b) -> (k, Monotype.UnsolvedType b)) kAbs

    _Γ <- get

    (_ΓR, _ΓL) <- Context.splitOnUnsolved DomainAlternatives p0 _Γ `orDie` MissingAllAlternatives p0 _Γ

    case rest of
        Left _ -> pure ()
        Right (Monotype.UnsolvedAlternatives p1) -> do
            p2 <- fresh

            set (_ΓR <> (Context.Solved DomainAlternatives p0 (Monotype.Alternatives kbs (Monotype.UnsolvedAlternatives p2)) : Context.Unsolved DomainAlternatives p2 : bs <> _ΓL))

            equateAlternatives p1 p2

        Right rest' -> do
            -- wellFormedType (bs <> _ΓL)
            --     Type.Union{ alternatives = Type.Alternatives [] rest, .. }
            -- FARENDE: ?

            set (_ΓR <> (Context.Solved DomainAlternatives p0 (Monotype.Alternatives kbs rest') : bs <> _ΓL))

    let instantiate (_, _A, b) = do
            _Θ <- get

            instantiateTypeR (RType _A0ctx _A) b

    traverse_ instantiate kAbs

evaluate :: MonadReader [Binding] m => Syntax Location (Type Location, Value) -> m Value
evaluate val = do
    b <- Reader.ask
    let constValues = b >>= \case
            BExpr name _ (Just value) -> [(name, value)]
            _nonKnownConstant -> []
    pure $ Normalize.evaluate constValues val

-- TODO: Simplify Status to mere bindings!
introduceVar :: MonadState Status m => Text -> Type' -> Maybe Value -> m (VariableId, [Binding])
introduceVar varName varType valM = do
    varTypeName <- VariableId "@" <$> freshInt
    pure (varTypeName, [BExpr varName varTypeName valM, BType varTypeName DomainType (Just varType)])

withVar :: (MonadReader [Binding] m, MonadState Status m) => Text -> Type' -> Maybe Value -> m a -> m (VariableId, m a)
withVar varName varType valM k = do
    (varTypeName, bindingEntries) <- introduceVar varName varType valM
    varTypeCaptured <- do
        ctx <- Reader.ask
        capturedType (ctx, varType)
    pure
        ( varTypeName
        , Reader.local (bindingEntries ++)
            $ scopedStack (pretty varName <> ": " <> pretty varTypeCaptured) k)

{-| This corresponds to the judgment:

    > Γ ⊢ e ⇒ A ⊣ Δ

    … which infers the type of e under input context Γ, producing an inferred
    type of A and an updated context Δ.
-}
infer
    :: (MonadReader [Binding] m, MonadState Status m, MonadError TypeInferenceErrorS m)
    => Syntax Location (Type Location, Value) -> m Type'
infer e0 = do
    let input ~> output = Type.Function{ location = Syntax.location e0, ..}
        mkForallWith domain name type_ = Type.Forall { location = Syntax.location e0, nameLocation = Syntax.location e0, .. }
        mkForallType name f = mkForallWith DomainType name $ f Type.Unresolved { location = Syntax.location e0, .. }
        mkForallAlternatives name f = mkForallWith DomainAlternatives name $ f $ Left name
        withVarInfer location varName varType valM k0 = do
            (varTypeName, k) <- withVar varName varType valM k0
            Type.Assign location varTypeName (Type.Binding DomainType varType) <$> k
        resolveCurrCtx type_ = (`RType` type_) <$> Reader.ask
        checkAnnotate expr type_ = check True expr =<< resolveCurrCtx type_
        checkVerify expr type_ = check False expr =<< resolveCurrCtx type_

    case e0 of
        -- Var
        Syntax.Variable{..} -> do
            ctx0 <- Reader.ask
            traceM "..."
            -- traceM $ show ctx0
            let assignmentsOfName = ctx0 >>= \case
                    BExpr name0 type0 _ | name == name0 -> [type0]
                    _notAssignmentOfName -> []
                (?!) [] _     = Nothing
                (?!) (x:_ ) 0 = Just x
                (?!) (_:xs) n = (?!) xs (n-1)
            t <- (?!) assignmentsOfName index `orDie` UnboundVariable location name index
            pure $ Unresolved location t

        -- →I⇒
        Syntax.Lambda{..} -> do
            a <- fresh
            b <- fresh

            let input = Type.UnsolvedType{ location = nameLocation, existential = a }

            let output = Type.UnsolvedType{ existential = b, .. }

            push (Context.Unsolved DomainType a)
            push (Context.Unsolved DomainType b)

            withVarInfer location name input Nothing do
                checkVerify body output
                pure Type.Function{..}

        -- →E
        Syntax.Application{..} -> do
            _A <- infer function


            rA <- resolveCurrCtx _A
            inferApplication rA argument

        -- Anno
        Syntax.Annotation{..} -> do
            _Γ <- get

            -- annotation <- resolveUnresolveds unresolvedAnnotation
            -- wellFormedType _Γ annotation
            -- FARENDE: ?

            checkAnnotate annotated annotation

            pure annotation

        Syntax.Let { bindings, body } -> do
            -- FARENDE: ?
            let process (Syntax.Binding { annotation = Nothing, ..}:xs) = do
                    _A <- infer assignment

                    -- push $ Context.Annotation name _A 

                    -- Reader.local (\b -> b { b_values = (name, _A, evaluate body):b_values b}) $
                    constVal <- evaluate body
                    withVarInfer nameLocation name _A (Just constVal) $ process xs
                    -- scoped (Context.Annotation name _A) $ process xs
                process (Syntax.Binding{ annotation = Just annotation, .. }:xs) = do
                    -- annotation <- resolveUnresolveds unresolvedAnnotation
                    checkAnnotate assignment annotation

                    -- Reader.local (\b -> b { b_values = (name, RType ctx0 annotation, evaluate body):b_values b}) $
                    constVal <- evaluate body
                    withVarInfer nameLocation name annotation (Just constVal) $ process xs
                    -- scoped (Context.Annotation name annotation) $ process xs
                -- process (Syntax.TypeBinding{ name = oldName, parameters = oldParameters, typeAssignment = oldType_, .. }:xs) = do
                --     -- TODO: Clear the output, do wellformed check right now, don't use scopedVariableType?
                --     let processParams (y : ys) type_ =
                --             scopedVariableType nameLocation DomainType (Monotype.variableId y) \newId substitute -> do
                --                 (newParams, r) <- processParams ys $ substitute type_
                --                 return (newId:newParams, r)
                --         processParams [] unresolvedType_ = do
                --             resolvedType_ <- resolveUnresolveds unresolvedType_
                --             return ([], resolvedType_)
                --     (newParameters, type_) <- processParams oldParameters oldType_
                --     introduceVariableId (Monotype.variableId oldName) \newName ->
                --         scoped (Context.Unresolved newName newParameters type_) do
                --             result <- process xs
                --             let replace location nameUnresolved parameters = case nameUnresolved of
                --                     Right name
                --                         | newName == name -> return type_
                --                     _ -> return $ Type.Unresolved {..}
                --             Type.substituteUnresolveds replace result
                process [] = infer body

            process (NonEmpty.toList bindings)
        Syntax.List{..} -> do
            case Seq.viewl elements of
                EmptyL -> do
                    existential <- fresh

                    push (Context.Unsolved DomainType existential)

                    pure Type.List { type_ = Type.UnsolvedType{..}, .. }
                y :< ys -> do
                    type_ <- infer y

                    traverse_ (`checkVerify` type_) ys

                    pure Type.List{..}

        Syntax.Record{..} -> do
            let process (field, value) = do
                    type_ <- infer value

                    return (field, type_)

            fieldTypes <- traverse process fieldValues

            return Type.Record{ fields = Type.Fields fieldTypes (Right Monotype.EmptyFields), .. }

        Syntax.Alternative{..} -> do
            existential <- fresh
            p           <- fresh

            push (Context.Unsolved DomainType existential)
            push (Context.Unsolved DomainAlternatives p)

            return
                (   Type.UnsolvedType{..}
                ~>  Type.Union
                        { alternatives = Type.Alternatives
                            [( name, Type.UnsolvedType{..})]
                            (Right $ Monotype.UnsolvedAlternatives p)
                        , ..
                        }
                )

        Syntax.Merge{..} -> do
            p <- fresh

            push (Context.Unsolved DomainFields p)
            let _R = Type.Fields [] (Right $ Monotype.UnsolvedFields p)

            checkVerify handlers $ Type.Record { location = Syntax.location handlers, fields = _R }-- $ Type.Record { location = Syntax.location handlers, fields = Type.Fields [] (Right $ Monotype.UnsolvedFields p)}

            -- We cannot just unwrap existential once to get the entirity
            -- of inerred record, unfortunately
            ctx0 <- Reader.ask
            (ctx, _R') <- Type.unwrapRecord location ctx0 _R

            case _R' of
                Type.Fields keyTypes (Right Monotype.EmptyFields) -> do
                    existential <- fresh

                    push (Context.Unsolved DomainType existential)

                    let process (key, Type.Function{ location = _, ..}) = do
                            let b' = Type.UnsolvedType{ location = Type.location output, .. }

                            subtype (RType ctx output) (RType ctx b')

                            return (key, input)
                        process (_, _A) = do
                            _Ac <- capturedRType (RType ctx _A)
                            throwErrorStack (MergeInvalidHandler (Type.location _A) _Ac)

                    keyTypes' <- traverse process keyTypes

                    return
                        (   Type.Union
                                { alternatives =
                                    Type.Alternatives
                                        keyTypes'
                                        (Right Monotype.EmptyAlternatives)
                                , ..
                                }
                        ~>      Type.UnsolvedType{..}
                        )

                Type.Fields _ _nonEmpty -> do
                    _R'c <- capturedType (ctx, _R')
                    throwErrorStack (MergeConcreteRecord location _R'c)
        Syntax.Field{..} -> do
            existential <- fresh
            p <- fresh

            push (Context.Unsolved DomainType existential)
            push (Context.Unsolved DomainFields p)

            checkVerify record $ Type.Record
                { fields =
                    Type.Fields
                        [(field, Type.UnsolvedType{..})]
                        (Right $ Monotype.UnsolvedFields p)
                , location = fieldLocation
                }

            return Type.UnsolvedType{ location = fieldLocation, ..}

        Syntax.If{..} -> do
            checkVerify predicate Type.Scalar{ scalar = Monotype.Bool, .. }

            _L0 <- infer ifTrue

            checkVerify ifFalse _L0

            pure _L0

        -- All the type inference rules for scalars go here.  This part is
        -- pretty self-explanatory: a scalar literal returns the matching
        -- scalar type.
        Syntax.Scalar{ scalar = Syntax.Bool _, .. } -> do
            return Type.Scalar{ scalar = Monotype.Bool, .. }

        Syntax.Scalar{ scalar = Syntax.Real _, .. } -> do
            return Type.Scalar{ scalar = Monotype.Real, .. }

        Syntax.Scalar{ scalar = Syntax.Integer _, .. } -> do
            return Type.Scalar{ scalar = Monotype.Integer, .. }

        Syntax.Scalar{ scalar = Syntax.Natural _, .. } -> do
            return Type.Scalar{ scalar = Monotype.Natural, .. }

        Syntax.Scalar{ scalar = Syntax.Text _, .. } -> do
            return Type.Scalar{ scalar = Monotype.Text, .. }

        Syntax.Scalar{ scalar = Syntax.Null, .. } -> do
            -- NOTE: You might think that you could just infer that `null`
            -- has type `forall (a : Type) . Optional a`.  This does not work
            -- because it will lead to data structures with impredicative types
            -- if you store a `null` inside of, say, a `List`.
            existential <- fresh

            push (Context.Unsolved DomainType existential)

            return Type.Optional{ type_ = Type.UnsolvedType{..}, .. }

        Syntax.Operator{ operator = Syntax.And, .. } -> do
            checkVerify left  Type.Scalar{ scalar = Monotype.Bool, location = operatorLocation }
            checkVerify right Type.Scalar{ scalar = Monotype.Bool, location = operatorLocation }

            return Type.Scalar{ scalar = Monotype.Bool, location = operatorLocation }

        Syntax.Operator{ operator = Syntax.Or, .. } -> do
            checkVerify left  Type.Scalar{ scalar = Monotype.Bool, location = operatorLocation }
            checkVerify right Type.Scalar{ scalar = Monotype.Bool, location = operatorLocation }

            return Type.Scalar{ scalar = Monotype.Bool, location = operatorLocation }

        -- FARENDE: I'm sceptical about this part
        Syntax.Operator{ operator = Syntax.Times, .. } -> do
            _L <- infer left
            _R <- infer right

            checkVerify left  _R
            checkVerify right _L

            ctx0 <- Reader.ask
            (ctx, _L') <- Type.unwrapType $ RType ctx0 _L

            case _L' of
                Type.Scalar{ scalar = Monotype.Natural } -> return _L
                Type.Scalar{ scalar = Monotype.Integer } -> return _L
                Type.Scalar{ scalar = Monotype.Real    } -> return _L
                _invalidOperand -> do
                    _L'c <- capturedRType $ RType ctx _L'
                    throwErrorStack (InvalidOperands (Syntax.location left) _L'c)

        Syntax.Operator{ operator = Syntax.Plus, .. } -> do
            _L <- infer left
            _R <- infer right

            checkVerify left  _R
            checkVerify right _L

            _Γ <- get

            ctx0 <- Reader.ask
            (ctx, _L') <- Type.unwrapType $ RType ctx0 _L

            case _L' of
                Type.Scalar{ scalar = Monotype.Natural } -> return _L
                Type.Scalar{ scalar = Monotype.Integer } -> return _L
                Type.Scalar{ scalar = Monotype.Real    } -> return _L
                Type.Scalar{ scalar = Monotype.Text    } -> return _L
                Type.List{}                              -> return _L

                _invalidOperand -> do
                    _L'c <- capturedRType $ RType ctx _L'
                    throwErrorStack (InvalidOperands (Syntax.location left) _L'c)

        Syntax.Builtin{ builtin = Syntax.RealEqual, .. }-> do
            return
                (   Type.Scalar{ scalar = Monotype.Real, .. }
                ~>  (   Type.Scalar{ scalar = Monotype.Real, .. }
                    ~>  Type.Scalar{ scalar = Monotype.Bool, .. }
                    )
                )

        Syntax.Builtin{ builtin = Syntax.RealLessThan, .. } -> do
            return
                (   Type.Scalar{ scalar = Monotype.Real, .. }
                ~>  (   Type.Scalar{ scalar = Monotype.Real, .. }
                    ~>  Type.Scalar{ scalar = Monotype.Bool, .. }
                    )
                )

        Syntax.Builtin{ builtin = Syntax.RealNegate, .. } -> do
            return
                (   Type.Scalar{ scalar = Monotype.Real, .. }
                ~>  Type.Scalar{ scalar = Monotype.Real, .. }
                )

        Syntax.Builtin{ builtin = Syntax.RealShow, .. } -> do
            return
                (   Type.Scalar{ scalar = Monotype.Real, .. }
                ~>  Type.Scalar{ scalar = Monotype.Text, .. }
                )

        Syntax.Builtin{ builtin = Syntax.ListDrop, .. } -> do
            pure $ mkForallType "a" \a ->
                Type.Scalar{ scalar = Monotype.Natural, .. }
                    ~>  (   Type.List{ type_ = a, .. }
                        ~>  Type.List{ type_ = a, .. }
                        )
        Syntax.Builtin{ builtin = Syntax.ListHead, .. } -> do
            pure $ mkForallType "a" \a ->
                mkForallAlternatives "b" \b ->
                    Type.List { type_ = a, .. }
                ~>  Type.Union
                        { alternatives =
                            Type.Alternatives
                                [ ("Some", a )
                                , ("None", Type.Record{ fields = Type.Fields [] $ Right Monotype.EmptyFields, .. })
                                ]
                                b
                        , ..
                        }
            -- return Type.Forall
            --     { nameLocation = Syntax.location e0
            --     , name = "a"
            --     , domain = DomainType
            --     , type_ =
            --         Type.Forall
            --             { nameLocation = Syntax.location e0
            --             , name = "b"
            --             , domain = DomainAlternatives
            --             , type_ =
                --         , ..
                --         }
                -- , ..
                -- }

        Syntax.Builtin{ builtin = Syntax.ListEqual, .. } -> do
            pure $ mkForallType "a" \a ->
                -- { nameLocation = Syntax.location e0
                -- , name = "a"
                -- , domain = DomainType
                -- , type_ =
                        (   a
                        ~>  (a ~> Type.Scalar{ scalar = Monotype.Bool, .. })
                        )
                    ~>  (   Type.List{ type_ = a, .. }
                        ~>  (   Type.List{ type_ = a, .. }
                            ~>  Type.Scalar{ scalar = Monotype.Bool, .. }
                            )
                        )
                -- , ..
                -- }

        Syntax.Builtin{ builtin = Syntax.ListFold, .. } -> do
            pure $ mkForallType "a" \a ->
                mkForallType "b" \b ->
            -- return Type.Forall
            --     { nameLocation = Syntax.location e0
            --     , name = "a"
            --     , domain = DomainType
            --     , type_ = Type.Forall
            --         { nameLocation = Syntax.location e0
            --         , name = "b"
            --         , domain = DomainType
            --         , type_ =
                            Type.Record
                                { fields =
                                    Type.Fields
                                        [ ("cons", a ~> (b ~> b))
                                        , ("nil", b)
                                        ]
                                        (Right Monotype.EmptyFields)
                                , ..
                                }
                        ~>  (Type.List{ type_= a, .. } ~> b)
                --     , ..
                --     }
                -- , ..
                -- }


        Syntax.Builtin{ builtin = Syntax.ListIndexed, .. } -> do
            -- return Type.Forall
            --     { nameLocation = Syntax.location e0
            --     , name = "a"
            --     , domain = DomainType
            --     , type_ =
            pure $ mkForallType "a" \a ->
                        Type.List{ type_ = a, .. }
                    ~>  Type.List
                            { type_ =
                                Type.Record
                                    { fields =
                                        Type.Fields
                                            [ ("index", Type.Scalar{ scalar = Monotype.Natural, .. })
                                            , ("value", a)
                                            ]
                                            (Right Monotype.EmptyFields)
                                    , ..
                                    }
                            , ..
                            }
                -- , ..
                -- }

        Syntax.Builtin{ builtin = Syntax.ListLast, .. } -> do
            -- return Type.Forall
            --     { nameLocation = Syntax.location e0
            --     , name = "a"
            --     , domain = DomainType
            --     , type_ = Type.Forall
            pure $ mkForallType "a" \a ->
                mkForallAlternatives "b" \b ->
                    -- { nameLocation = Syntax.location e0
                    -- , name = "b"
                    -- , domain  = DomainAlternatives
                    -- , type_ =
                            Type.List{ type_ = a, .. }
                        ~>  Type.Union
                                { alternatives =
                                    Type.Alternatives
                                        [ ("Some", a)
                                        , ("None", Type.Record{ fields = Type.Fields [] (Right Monotype.EmptyFields), .. })
                                        ]
                                        b
                                , ..
                                }
                --     , ..
                --     }
                -- , ..
                -- }

        Syntax.Builtin{ builtin = Syntax.ListLength, .. } -> do
            -- return Type.Forall
            --     { nameLocation = Syntax.location e0
            --     , name = "a"
            --     , domain = DomainType
            --     , type_ =
            pure $ mkForallType "a" \a ->
                        Type.List{ type_ = a, .. }
                    ~>  Type.Scalar{ scalar = Monotype.Natural, .. }
                -- , ..
                -- }

        Syntax.Builtin{ builtin = Syntax.ListMap, .. } -> do
            pure $ mkForallType "a" \a ->
                mkForallType "b" \b ->

            -- return Type.Forall
            --     { nameLocation = Syntax.location e0
            --     , name = "a"
            --     , domain = DomainType
            --     , type_ = Type.Forall
            --         { nameLocation = Syntax.location e0
            --         , name = "b"
            --         , domain = DomainType
            --         , type_ =
                            (a ~> b)
                        ~>  (   Type.List{ type_ = a, .. }
                            ~>  Type.List{ type_ = b, .. }
                            )
                --     , ..
                --     }
                -- , ..
                -- }

        Syntax.Builtin{ builtin = Syntax.IntegerAbs, .. } -> do
            return
                (   Type.Scalar{ scalar = Monotype.Integer, .. }
                ~>  Type.Scalar{ scalar = Monotype.Natural, .. }
                )

        Syntax.Builtin{ builtin = Syntax.IntegerEven, .. } -> do
            return
                (   Type.Scalar{ scalar = Monotype.Integer, .. }
                ~>  Type.Scalar{ scalar = Monotype.Bool, .. }
                )

        Syntax.Builtin{ builtin = Syntax.IntegerNegate, .. } -> do
            return
                (   Type.Scalar{ scalar = Monotype.Integer, .. }
                ~>  Type.Scalar{ scalar = Monotype.Integer, .. }
                )

        Syntax.Builtin{ builtin = Syntax.IntegerOdd, .. } -> do
            return
                (   Type.Scalar{ scalar = Monotype.Integer, .. }
                ~>  Type.Scalar{ scalar = Monotype.Bool, .. }
                )

        Syntax.Builtin{ builtin = Syntax.ListReverse, .. } -> do
            pure $ mkForallType "a" \a -> Type.List{ type_ = a, .. } ~> Type.List{ type_ = a, .. }
            -- return Type.Forall
            --     { nameLocation = Syntax.location e0
            --     , name = "a"
            --     , domain = DomainType
                -- , type_ = Type.List{ type_ = var "a", .. } ~> Type.List{ type_ = var "a", .. }
                -- , ..
                -- }

        Syntax.Builtin{ builtin = Syntax.ListTake, .. } -> do
            pure $ mkForallType "a" \a ->
            -- return Type.Forall
            --     { nameLocation = Syntax.location e0
            --     , name = "a"
            --     , domain = DomainType
            --     , type_ =
                        Type.Scalar{ scalar = Monotype.Natural, .. }
                    ~>  (   Type.List{ type_ = a, .. }
                        ~>  Type.List{ type_ = a, .. }
                        )
                -- , ..
                -- }

        Syntax.Builtin{ builtin = Syntax.JSONFold, .. } -> do
            pure $ mkForallType "a" \a ->
            -- return Type.Forall
            --     { nameLocation = Syntax.location e0
            --     , name = "a"
            --     , domain = DomainType
            --     , type_ =
                        Type.Record
                            { fields = Type.Fields
                                [ ( "array", Type.List{ type_ = a, .. } ~> a)
                                , ("bool", Type.Scalar{ scalar = Monotype.Bool, .. } ~> a)
                                , ("real", Type.Scalar{ scalar = Monotype.Real, ..  } ~> a)
                                , ("integer", Type.Scalar{ scalar = Monotype.Integer, .. } ~> a)
                                , ("natural", Type.Scalar{ scalar = Monotype.Natural, .. } ~> a)
                                , ("null", a)
                                , ( "object"
                                  ,     Type.List
                                            { type_ = Type.Record
                                                { fields =
                                                    Type.Fields
                                                        [ ("key", Type.Scalar{ scalar = Monotype.Text, .. })
                                                        , ("value", a)
                                                        ]
                                                        (Right Monotype.EmptyFields)
                                                , ..
                                                }
                                            , ..
                                            }
                                    ~>  a
                                  )
                                , ("string", Type.Scalar{ scalar = Monotype.Text, .. } ~> a)
                                ]
                                (Right Monotype.EmptyFields)
                            , ..
                            }
                    ~>  (   Type.Scalar{ scalar = Monotype.JSON, .. }
                        ~>  a
                        )
                -- , ..
                -- }

        Syntax.Builtin{ builtin = Syntax.NaturalFold, .. } -> do
            pure $ mkForallType "a" \a ->
            -- return Type.Forall
            --     { nameLocation = Syntax.location e0
            --     , name = "a"
            --     , domain = DomainType
            --     , type_ =
                        Type.Scalar{ scalar = Monotype.Natural, .. }
                    ~>  ((a ~> a) ~> (a ~> a))
                -- , ..
                -- }

        Syntax.Builtin{ builtin = Syntax.TextEqual, .. } -> do
            return
                (   Type.Scalar{ scalar = Monotype.Text, .. }
                ~>  (   Type.Scalar{ scalar = Monotype.Text, .. }
                    ~>  Type.Scalar{ scalar = Monotype.Bool, .. }
                    )
                )

        Syntax.Embed{ embedded = (type_, _) } -> do
            return type_

-- FARENDE: check _ (Forall ...) extends [Binding] of expression, which might not be desirable!
{-| This corresponds to the judgment:

    > Γ ⊢ e ⇐ A ⊣ Δ

    … which checks that e has type A under input context Γ, producing an updated
    context Δ.
-}
check
    :: (MonadReader [Binding] m, MonadState Status m, MonadError TypeInferenceErrorS m)
    => Bool -> Syntax Location (Type Location, Value) -> RType' -> m ()
check annotate e0 y0 = do
    (yCtx, yType) <- Type.unwrapType y0
    capturedY <- capturedRType y0
    scopedStack ("<check> : " <> pretty capturedY) case (e0, yType) of
        -- The check function is the most important function to understand for the
        -- bidirectional type-checking algorithm.
        --
        -- Most people, when they first run across the `check` function think that you
        -- could get rid of most rules except for the final `Sub` rule, but that's not
        -- true!
        --
        -- The reason you should add `check` rules for many more types (especially
        -- complex types) is to ensure that subtyping rules work correctly.  For
        -- example, consider this expression:
        --
        --     [ 2, -3 ]
        --
        -- If you omit the `check` rule for `List`s then the above expression will
        -- fail to type-check because the first element of the list is a `Natural`
        -- number and the second element of the `List` is an `Integer`.
        --
        -- However, if you keep the `check` rule for `List`s and add a type annotation:
        --
        --     [ 2, -3 ] : List Integer
        --
        -- … then it works because the interpreter knows to treat both elements as an
        -- `Integer`.
        --
        -- In general, if you want subtyping to work reliably then you need to add
        -- more cases to the `check` function so that the interpreter can propagate
        -- top-level type annotations down to the "leaves" of your syntax tree.  If
        -- you do this consistently then the user only ever needs to provide top-level
        -- type annotations to fix any type errors that they might encounter, which is
        -- a desirable property!

        -- →I
        (Syntax.Lambda{ location = _, ..}, Type.Function{..}) -> do
            (_, act) <- withVar name input Nothing do
                traceM $ "WORKS FOR " <> show name
                check annotate body (RType yCtx output)
            act

        -- ∃I
        (e, Type.Exists{..}) ->
            scopedUnsolvedType nameLocation domain name \upd ->
                Reader.local (if annotate then upd else id) $ check annotate e $ RType (upd yCtx) type_

        -- ∀I
        (e, Type.Forall{..}) ->
            scopedVariableType nameLocation domain name \upd ->
                Reader.local (if annotate then upd else id) $ check annotate e $ RType (upd yCtx) type_

        (Syntax.Operator{ operator = Syntax.Times, .. }, _B@Type.Scalar{ scalar })
            | scalar `elem` ([ Monotype.Natural, Monotype.Integer, Monotype.Real ] :: [Monotype.Scalar]) -> do
                check annotate left $ RType yCtx _B
                check annotate right $ RType yCtx _B
        (Syntax.Operator{ operator = Syntax.Plus, .. }, _B@Type.Scalar{ scalar })
            | scalar `elem` ([ Monotype.Natural, Monotype.Integer, Monotype.Real, Monotype.Text ] :: [Monotype.Scalar]) -> do
                check annotate left $ RType yCtx _B
                check annotate right $ RType yCtx _B
        (Syntax.Operator{ operator = Syntax.Plus, .. }, _B@Type.List{}) -> do
            check annotate left $ RType yCtx _B
            check annotate right $ RType yCtx _B

        (Syntax.List{ location = _, ..}, Type.List{..}) ->
            traverse_ (\x -> check annotate x $ RType yCtx type_) elements

        (e@Syntax.Record{ fieldValues }, _B@Type.Record{ fields = Type.Fields fieldTypes fields })
            | let mapValues = Map.fromList fieldValues
            , let mapTypes  = Map.fromList fieldTypes

            , let extraValues = Map.difference mapValues mapTypes
            , let extraTypes  = Map.difference mapTypes  mapValues

            , let both = Map.intersectionWith (,) mapValues mapTypes
            , not (Map.null both) -> do
                let process (value, type_) = check annotate value $ RType yCtx type_

                traverse_ process both

                let e' = Syntax.Record
                        { fieldValues = Map.toList extraValues
                        , location = Syntax.location e
                        }

                let _B' = Type.Record
                        { location = Type.location _B
                        , fields = Type.Fields (Map.toList extraTypes) fields
                        }

                check annotate e' $ RType yCtx _B'

        (Syntax.Record{..}, _B@Type.Scalar{ scalar = Monotype.JSON }) -> do
            let process (_, value) = check annotate value (RType yCtx _B)

            traverse_ process fieldValues
        (Syntax.List{..}, _B@Type.Scalar{ scalar = Monotype.JSON }) -> do
            traverse_ (\x -> check annotate x $ RType yCtx _B) elements
        (Syntax.Scalar{ scalar = Syntax.Text _ }, Type.Scalar{ scalar = Monotype.JSON }) -> do
            return ()
        (Syntax.Scalar{ scalar = Syntax.Natural _ }, Type.Scalar{ scalar = Monotype.JSON }) -> do
            return ()
        (Syntax.Scalar{ scalar = Syntax.Integer _ }, Type.Scalar{ scalar = Monotype.JSON }) -> do
            return ()
        (Syntax.Scalar{ scalar = Syntax.Real _ }, Type.Scalar{ scalar = Monotype.JSON }) -> do
            return ()
        (Syntax.Scalar{ scalar = Syntax.Bool _ }, Type.Scalar{ scalar = Monotype.JSON }) -> do
            return ()
        (Syntax.Scalar{ scalar = Syntax.Null }, Type.Scalar{ scalar = Monotype.JSON }) -> do
            return ()

        (e, _B) -> do
            _A <- infer e

            ctx0 <- Reader.ask

            subtype (RType ctx0 _A) (RType ctx0 _B)

-- FARENDE: `check` is not valid here, look at FARENDE of check
{-| This corresponds to the judgment:

    > Γ ⊢ A • e ⇒⇒ C ⊣ Δ

    … which infers the result type C when a function of type A is applied to an
    input argument e, under input context Γ, producing an updated context Δ.
-}

-- TODO: WHEN PERFORMING MULTIPLE CHECKS AT A SAME PLACE, WE DON'T ALWAYS USE RTYPE!
inferApplication
    :: (MonadReader [Binding] m, MonadState Status m, MonadError TypeInferenceErrorS m)
    => RType'
    -> Syntax Location (Type Location, Value)
    -> m Type'
inferApplication rA0 e = do
    do
        t <- capturedRType rA0
        traceM $ show $ "inferApplication " <> pretty t
    (_A0ctx, _A0type) <- Type.unwrapType rA0
    case _A0type of
        -- ∀App
        Type.Forall{..} -> do
            (_, entry, bind@(Type.Binding domain2 value)) <- unsolvedType location domain
            push entry
            Type.Assign location name bind <$> inferApplication (RType (BType name domain2 (Just value) :_A0ctx) type_) e

        -- FARENDE: ?
        -- ∃App
        Type.Exists{..} -> do
            -- TODO: There appears to be a bug in Grace repo. Scoped is used there. But why scoped?
            (entry, bind@(Type.Binding domain2 value)) <- variableType nameLocation domain
            push entry
            Type.Assign location name bind <$> inferApplication (RType (BType name domain2 (Just value) : _A0ctx) type_) e

        -- αApp
        Type.UnsolvedType{ existential = a, .. } -> do
            _Γ <- get

            (_ΓR, _ΓL) <- Context.splitOnUnsolved DomainType a _Γ `orDie` MissingVariable a _Γ

            a1 <- fresh
            a2 <- fresh

            set (_ΓR <> (Context.Solved DomainType a (Monotype.Function (Monotype.UnsolvedType a1) (Monotype.UnsolvedType a2)) : Context.Unsolved DomainType a1 : Context.Unsolved DomainType a2 : _ΓL))

            check False e (RType _A0ctx Type.UnsolvedType{ existential = a1, .. })

            pure Type.UnsolvedType{ existential = a2, .. }

        Type.Function{..} -> do
            check False e (RType _A0ctx input)
            pure output -- HERE IT RETURNS NON-RESOLVED VALUE!!! AAAAA!!

        Type.VariableType{..} -> do
            throwErrorStack $ NotNecessarilyFunctionType location

        _A -> do
            ctx <- Reader.ask
            _Ac <- capturedRType $ RType ctx _A
            throwErrorStack (NotFunctionType (location _A) _Ac)

-- -- ∀App
-- inferApplication Type.Forall{ domain = DomainType, .. } -> do
--     a <- fresh

--     push (Context.Unsolved DomainType a)

--     let a' = Type.UnsolvedType{ location = nameLocation, existential = a}

--     inferApplication (Type.substituteType name a' type_) e
-- inferApplication Type.Forall{ domain = DomainFields, .. } e = do
--     a <- fresh

--     push (Context.Unsolved DomainFields a)

--     let a' = Type.Fields [] (Monotype.UnsolvedFields a)

--     inferApplication (Type.substituteFields name a' type_) e
-- inferApplication Type.Forall{ domain = DomainAlternatives, .. } e = do
--     a <- fresh

--     push (Context.Unsolved DomainAlternatives a)

--     let a' = Type.Alternatives [] (Monotype.UnsolvedAlternatives a)

--     inferApplication (Type.substituteAlternatives name a' type_) e

-- -- ∃App
-- inferApplication Type.Exists{..} e = do
--     scopedVariableType nameLocation domain name \_ substitute ->
--         inferApplication (substitute type_) e

-- -- αApp
-- inferApplication Type.UnsolvedType{ existential = a, .. } e = do
--     _Γ <- get

--     (_ΓR, _ΓL) <- Context.splitOnUnsolved DomainType a _Γ `orDie` MissingVariable a _Γ

--     a1 <- fresh
--     a2 <- fresh

--     set (_ΓR <> (Context.Solved DomainType a (Monotype.Function (Monotype.UnsolvedType a1) (Monotype.UnsolvedType a2)) : Context.Unsolved DomainType a1 : Context.Unsolved DomainType a2 : _ΓL))

--     check e Type.UnsolvedType{ existential = a1, .. }

--     return Type.UnsolvedType{ existential = a2, .. }
-- inferApplication Type.Function{..} e = do
--     check e input

--     return output
-- inferApplication Type.VariableType{..} _ = do
--     throwErrorStack (NotNecessarilyFunctionType location name)
-- inferApplication _A _ = do
--     throwErrorStack (NotFunctionType (location _A) _A)

-- | Infer the `Type` of the given `Syntax` tree with these 
typeOf
    :: Syntax Location (Type Location, Value)
    -> Either TypeInferenceErrorS (Type Location)
typeOf = typeWith [] -- Context [] Map.empty)

-- | Like `typeOf`, but accepts a custom type-checking `[Entry]`
typeWith
    :: [(Text, Type', Value)]
    -> Syntax Location (Type Location, Value)
    -> Either TypeInferenceErrorS (Type Location)
typeWith rawBinding syntax = do
    (join -> bindingEntries, initialStatus) <- State.runStateT
        (forM rawBinding \(a, b, c) -> snd <$> introduceVar a b (Just c))
        (Status{ count = 0, context = Context [] Map.empty, stack = Just (StackZipper [] Nothing) })

    (_A, _Δ) <- State.runStateT (Reader.runReaderT (infer syntax) bindingEntries) initialStatus
    -- TODO: push "complete" onto stack

    complete _Δ (RType bindingEntries _A)

{-| This function is used at the end of the bidirectional type-checking
    algorithm to complete the inferred type by:

    * Substituting the type with the solved entries in the `Context`

    * Adding universal quantifiers for all unsolved entries in the `Context`

    >>> original = Type.Function () (Type.UnsolvedType () 1) (Type.UnsolvedType () 0)
    >>> pretty @(Type ()) original
    b? -> a?

    >>> context = Context [ UnsolvedType 1, SolvedType 0 (Monotype.Scalar Monotype.Bool) ] (Map.fromList [ ("a", 3) ])
    >>> pretty @(Type ()) (complete context original)
    forall (a3 : Type) . a3 -> Bool
-}
    -- :: (MonadState Status m, MonadError TypeInferenceErrorS m)

-- FARENDE: REASSIGN ids of VariableIds with nested VariableId of the same name.
-- :t \x -> ((\y -> y) : forall (a : Type). a -> a) x
-- Fixes forall a. assign @b : a. forall a. @b
complete :: forall m. MonadError TypeInferenceErrorS m => Status -> RType' -> m Type'
complete state = \t ->
    -- FARENDE: Handle exists!
    State.evalStateT (State.runStateT (Reader.runReaderT (getCompose $ f t) HM.empty) (HM.fromList $ (\x -> (x, 0)) <$> ['a'..'z'])) state
    <&> \(inject, firstFrees) ->
        let assigned = HM.empty
            free = foldl' (\acc (chr, firstFree) -> insertFree chr firstFree acc) Map.empty (HM.toList firstFrees)
            (type0, (assignments, _)) = State.runState inject (assigned, free)
        in foldl'
            (\type_ ((domain, _, isForall), name) ->
                (if isForall then Type.Forall else Type.Exists)
                    (Type.location type_) (Type.location type_) name domain type_)
            type0
            (HM.toList assignments)
  where
    -- Invariant: Map Int is not empty and contains non-empty Set Char
    insertFree :: Char -> Int -> Map Int (Set Char) -> Map Int (Set Char)
    insertFree chr = Map.alter (Just . Set.insert chr . fromMaybe Set.empty)

    -- Just declares type variable of some domain, without setting it to some concrete type.
    declareUnsolved :: VariableId -> Domain' -> [Binding] -> [Binding]
    declareUnsolved name = \case
        DomainType -> (BType name DomainType Nothing:)
        DomainFields -> (BType name DomainFields Nothing:)
        DomainAlternatives -> (BType name DomainAlternatives Nothing:)

    -- StateT Status -- type-level bindings
    -- StateT (HashMap Char Int) -- first free id for char from a..z to use
    -- ReaderT (HashMap Text Int) -- first free id for string in current path.
    -- State (HashMap (Domain, Int, Bool) VariableId, Map Int (Set Char)) -- (introduced type variables (True for Forall, False for Exists), not yet used type variables)
    f :: RType' ->
        Compose
            (ReaderT (HashMap Text Int) (StateT (HashMap Char Int) (StateT Status m)))
            (State (HashMap (Domain', Int, Bool) VariableId, Map Int (Set Char)))
            Type'
    f rt = Compose do
        (tctx, t) <- lift $ lift $ Type.unwrapType rt
        let
            assignToExistential domain existential isForall = do
                (assigned0, free0) <- State.get
                let (name, assigned, free) = case HM.lookup (domain, existential, isForall) assigned0 of
                        Just x -> (x, assigned0, free0)
                        Nothing ->
                            let iamstupid = "Grace.Context.complete: Internal error - Unable to find unused VariableId's to complete the type"
                                (toUse, chars) = case Map.toAscList free0 of
                                    (a, b):_ -> (a, b)
                                    _noFreeImpossible -> error iamstupid
                                char = case Set.toAscList chars of
                                    c:_ -> c
                                    _nonEmpty -> error iamstupid
                                variableId = VariableId (Text.singleton char) toUse
                            in ( variableId
                                , HM.insert (domain, existential, isForall) variableId assigned0
                                , Map.update ((\new -> if Set.null new then Nothing else Just new) . Set.delete char) toUse $
                                    insertFree char (toUse + 1) free0)
                State.put (assigned, free)
                pure name
            handleExistsForall constructor nameLocation location (VariableId label oldInd) domain oldType = Compose do
                let inner name tctxUpd = getCompose $
                        constructor nameLocation location name domain
                        <$> f (Type.RType (tctxUpd $ declareUnsolved name domain tctx) oldType)
                free <- Reader.ask
                let firstFree = fromMaybe 0 $ HM.lookup label free
                Reader.local (\_ -> HM.insert label (firstFree + 1) free)
                    let oldVId = VariableId label oldInd in
                    if oldInd >= firstFree
                        then inner oldVId id
                        else
                            let newVId = VariableId label firstFree
                                rename = case domain of
                                    DomainType -> BType oldVId DomainType (Just $ Type.Unresolved location newVId)
                                    DomainFields -> BType oldVId DomainFields (Just $ Type.Fields [] $ Left newVId)
                                    DomainAlternatives -> BType oldVId DomainAlternatives (Just $ Type.Alternatives [] $ Left newVId)
                            in inner newVId (rename:)
        getCompose $ case t of
            Type.UnsolvedType { existential = UnsafeExistential existential, .. } -> Compose $ pure do
                name <- assignToExistential DomainType existential True
                pure $ Type.Unresolved {..}
            Type.VariableType { existential = UnsafeExistential existential, .. } -> Compose $ pure do
                name <- assignToExistential DomainType existential False
                pure $ Type.Unresolved {..}
            Type.Exists {..} -> handleExistsForall Type.Exists nameLocation location name domain type_
            Type.Forall {..} -> handleExistsForall Type.Forall nameLocation location name domain type_
            Type.Assign { type_ = old } -> f (Type.RType tctx old)
            Type.Function { input = oldInput, output = oldOutput, ..} ->
                (\input output -> Type.Function {..})
                    <$> f (Type.RType tctx oldInput)
                    <*> f (Type.RType tctx oldOutput)
            Type.Optional { type_ = old, ..} -> (\type_ -> Type.Optional {..}) <$> f (Type.RType tctx old)
            Type.List { type_ = old, ..} -> (\type_ -> Type.List {..}) <$> f (Type.RType tctx old)
            Type.Record { fields = Type.Fields fields0 remaining0, .. } ->
                (\fields_ remaining_ -> Type.Record { fields = Type.Fields fields_ remaining_, .. })
                    <$> for fields0 (\(fieldName, fieldType0) -> (\fieldType -> (fieldName, fieldType)) <$> f (Type.RType tctx fieldType0))
                    <*> Compose (pure case remaining0 of
                            Right (Monotype.UnsolvedFields (UnsafeExistential existential)) -> do
                                name <- assignToExistential DomainFields existential True
                                pure $ Left name
                            Right (Monotype.VariableFields (UnsafeExistential existential)) -> do
                                name <- assignToExistential DomainFields existential False
                                pure $ Left name
                            _emptyOrUnresolved -> pure remaining0)
            Type.Union { alternatives = Type.Alternatives alternatives0 remaining0, .. } ->
                (\alternatives_ remaining_ -> Type.Union { alternatives = Type.Alternatives alternatives_ remaining_, .. })
                    <$> for alternatives0 (\(fieldName, fieldType0) -> (\fieldType -> (fieldName, fieldType)) <$> f (Type.RType tctx fieldType0))
                    <*> Compose (pure case remaining0 of
                            Right (Monotype.UnsolvedAlternatives (UnsafeExistential existential)) -> do
                                name <- assignToExistential DomainAlternatives existential False
                                pure $ Left name
                            Right (Monotype.VariableAlternatives (UnsafeExistential existential)) -> do
                                name <- assignToExistential DomainAlternatives existential False
                                pure $ Left name
                            _emptyOrUnresolved -> pure remaining0)
            Type.Unresolved {} -> pure t
            Type.Scalar {} -> pure t

-- FARENDE: use captured type
-- | A data type holding all errors related to type inference
data TypeInferenceError
    = IllFormedAlternatives Location (Existential Monotype.Union) ([Entry])
    | IllFormedFields Location (Existential Monotype.Record) ([Entry])
    | IllFormedType Location (CapturedType Type Location) ([Entry])
    --
    | InvalidOperands Location (CapturedType Type Location)
    --
    | MergeConcreteRecord Location (CapturedType Type.Record Location)
    | MergeInvalidHandler Location (CapturedType Type Location)
    | MergeRecord Location (CapturedType Type Location)
    --
    | MissingAllAlternatives (Existential Monotype.Union) ([Entry])
    | MissingAllFields (Existential Monotype.Record) ([Entry])
    | MissingOneOfAlternatives [Location] (Existential Monotype.Union) (Existential Monotype.Union) ([Entry])
    | MissingOneOfFields [Location] (Existential Monotype.Record) (Existential Monotype.Record) ([Entry])
    | MissingVariable (Existential Monotype) ([Entry])
    | MissingUnresolvedResolution Location Text
    --
    | NotFunctionType Location (CapturedType Type Location)
    | NotNecessarilyFunctionType Location
    --
    -- | NotAlternativesSubtype Location (Existential Monotype.Union) (Type.Union Location)
    -- | NotFieldsSubtype Location (Existential Monotype.Record) (Type.Record Location)
    | NotRecordSubtype Location (CapturedType Type Location) Location (CapturedType Type Location)
    | NotUnionSubtype Location (CapturedType Type Location) Location (CapturedType Type Location)
    | NotSubtype Location (CapturedType Type Location) Location (CapturedType Type Location)
    --
    | UnboundAlternatives Location VariableId
    | UnboundFields Location VariableId
    | UnboundTypeVariable Location VariableId
    -- | UnboundUnresolved Location Text
    | UnboundVariable Location Text Int
    --
    | RecordTypeMismatch (CapturedType Type Location) (CapturedType Type Location) (Map.Map Text (CapturedType Type Location)) (Map.Map Text (CapturedType Type Location))
    | UnionTypeMismatch (CapturedType Type Location) (CapturedType Type Location) (Map.Map Text (CapturedType Type Location)) (Map.Map Text (CapturedType Type Location))
    -- | UnresolvedMismatch -- TODO
    deriving Show

instance Exception TypeInferenceError where
    displayException (IllFormedAlternatives location a0 _Γ) =
        "Internal error: Invalid context\n\
        \\n\
        \The following unsolved alternatives variable:\n\
        \\n\
        \" <> insert (Context.Unsolved DomainAlternatives a0) <> "\n\
        \\n\
        \… is not well-formed within the following context:\n\
        \\n\
        \#{listToText _Γ}\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" location)

    displayException (IllFormedFields location a0 _Γ) =
        "Internal error: Invalid context\n\
        \\n\
        \The following unsolved fields variable:\n\
        \\n\
        \" <> insert (Context.Unsolved DomainFields a0) <> "\n\
        \\n\
        \… is not well-formed within the following context:\n\
        \\n\
        \" <> listToText _Γ <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" location)

    displayException (IllFormedType location _A _Γ) =
        "Internal error: Invalid context\n\
        \\n\
        \The following type:\n\
        \\n\
        \" <> insert _A <> "\n\
        \\n\
        \… is not well-formed within the following context:\n\
        \\n\
        \" <> listToText _Γ <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" location)

    displayException (InvalidOperands location _L') =
        "Invalid operands\n\
        \\n\
        \You cannot add values of type:\n\
        \\n\
        \" <> insert _L' <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" location)

    displayException (MergeConcreteRecord location _R) =
        "Must merge a concrete record\n\
        \\n\
        \The first argument to a merge expression must be a record where all fields are\n\
        \statically known.  However, you provided an argument of type:\n\
        \\n\
        \" <> insert _R <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" location) <> "\n\
        \\n\
        \… where not all fields could be inferred."

    displayException (MergeInvalidHandler location _A) =
        "Invalid handler\n\
        \\n\
        \The merge keyword expects a record of handlers where all handlers are functions,\n\
        \but you provided a handler of the following type:\n\
        \\n\
        \" <> insert _A <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" location) <> "\n\
        \\n\
        \… which is not a function type."

    displayException (MergeRecord location _R) =
        "Must merge a record\n\
        \\n\
        \The first argument to a merge expression must be a record, but you provided an\n\
        \expression of the following type:\n\
        \\n\
        \" <> insert _R <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" location) <> "\n\
        \\n\
        \… which is not a record type."

    displayException (MissingAllAlternatives p0 _Γ) =
        "Internal error: Invalid context\n\
        \\n\
        \The following unsolved alternatives variable:\n\
        \\n\
        \" <> insert (Context.Unsolved DomainAlternatives p0) <> "\n\
        \\n\
        \… cannot be instantiated because the alternatives variable is missing from the\n\
        \context:\n\
        \\n\
        \" <> listToText _Γ

    displayException (MissingAllFields p0 _Γ) =
        "Internal error: Invalid context\n\
        \\n\
        \The following unsolved fields variable:\n\
        \\n\
        \" <> insert (Context.Unsolved DomainFields p0) <> "\n\
        \\n\
        \… cannot be instantiated because the fields variable is missing from the\n\
        \context:\n\
        \\n\
        \" <> listToText _Γ

    displayException (MissingOneOfAlternatives locations p0 p1 _Γ) =
        "Internal error: Invalid context\n\
        \\n\
        \One of the following alternatives variables:\n\
        \\n\
        \" <> listToText [Context.Unsolved DomainAlternatives p0, Context.Unsolved DomainAlternatives p1 ] <> "\n\
        \\n\
        \… is missing from the following context:\n\
        \\n\
        \" <> listToText _Γ <> "\n\
        \\n\
        \" <> locations'
        where
            locations' =
                Text.unpack (Text.unlines (map (Location.renderError "") locations))

    displayException (MissingOneOfFields locations p0 p1 _Γ) =
        "Internal error: Invalid context\n\
        \\n\
        \One of the following fields variables:\\n\
        \\n\
        \" <> listToText [Context.Unsolved DomainFields p0, Context.Unsolved DomainFields p1 ] <> "\n\
        \\n\
        \… is missing from the following context:\n\
        \\n\
        \" <> listToText _Γ <> "\n\
        \\n\
        \" <> locations'
        where
            locations' =
                Text.unpack (Text.unlines (map (Location.renderError "") locations))

    displayException (MissingVariable a _Γ) =
        "Internal error: Invalid context\n\
        \\n\
        \The following unsolved variable:\n\
        \\n\
        \" <> insert (Context.Unsolved DomainType a) <> "\n\
        \\n\
        \… cannot be solved because the variable is missing from the context:\n\
        \\n\
        \" <> listToText _Γ

        -- \expression of the following type:\n\
        -- \\n\
        -- \" <> insert _R <> "\n\
        -- \\n\
        -- \" <> Text.unpack (Location.renderError "" location) <> "\n\
        -- \\n\
        -- \… which is not a record type."
    displayException (MissingUnresolvedResolution location name) =
        "Internal error: Invalid context\n\
        \\n\
        \The following user type:\n\
        \\n\
        \" <> insert name <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" location) <> "\n\
        \\n\
        \… which is not a record type."

    displayException (NotFunctionType location _A) =
        "Not a function type\n\
        \\n\
        \An expression of the following type:\n\
        \\n\
        \" <> insert _A <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" location) <> "\n\
        \\n\
        \… was invoked as if it were a function, but the above type is not a function\n\
        \type."

    displayException (NotNecessarilyFunctionType location) =
        "Not necessarily a function type\n\
        \\n\
        \Type variable:  could potentially be any type and is not necessarily a function type.\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" location)
        -- TODO?

    -- displayException (NotAlternativesSubtype location p0 alternatives) =
    --     "Not an alternatives subtype\n\
    --     \\n\
    --     \The following alternatives variable:\n\
    --     \\n\
    --     \" <> insert p0 <> "\n\
    --     \\n\
    --     \… cannot be instantiated to the following union type:\n\
    --     \\n\
    --     \" <> insert (Type.Union location alternatives) <> "\n\
    --     \\n\
    --     \" <> Text.unpack (Location.renderError "" location) <> "\n\
    --     \\n\
    --     \… because the same alternatives variable appears within that union type."

    -- displayException (NotFieldsSubtype location p0 fields) =
    --     "Not a fields subtype\n\
    --     \\n\
    --     \The following fields variable:\n\
    --     \\n\
    --     \" <> insert p0 <> "\n\
    --     \\n\
    --     \… cannot be instantiated to the following record type:\n\
    --     \\n\
    --     \" <> insert (Type.Record location fields) <> "\n\
    --     \\n\
    --     \" <> Text.unpack (Location.renderError "" location) <> "\n\
    --     \\n\
    --     \… because the same fields variable appears within that record type."

    displayException (NotRecordSubtype locA0 _A locB0 _B) =
        "Not a record subtype\n\
        \\n\
        \The following type:\n\
        \\n\
        \" <> insert _A <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" locA0) <> "\n\
        \\n\
        \… cannot be a subtype of:\n\
        \\n\
        \" <> insert _B <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" locB0)

    displayException (NotUnionSubtype locA0 _A locB0 _B) =
        "Not a union subtype\n\
        \\n\
        \The following type:\n\
        \\n\
        \" <> insert _A <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" locA0) <> "\n\
        \\n\
        \… cannot be a subtype of:\n\
        \\n\
        \" <> insert _B <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" locB0)

    displayException (NotSubtype locA0 _A locB0 _B) =
        "Not a subtype\n\
        \\n\
        \The following type:\n\
        \\n\
        \" <> insert _A <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" locA0) <> "\n\
        \\n\
        \… cannot be a subtype of:\n\
        \\n\
        \" <> insert _B <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" locB0)

    displayException (UnboundAlternatives location a) =
        "Unbound alternatives variable: " <> Text.unpack (prettyToText a) <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" location)

    displayException (UnboundFields location a) =
        "Unbound fields variable: " <> Text.unpack (prettyToText a) <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" location)

    displayException (UnboundTypeVariable location a) =
        "Unbound type variable: " <> Text.unpack (prettyToText a) <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" location)

    -- displayException (UnboundUnresolved location a) =
    --     "Unbound user type variable: " <> Text.unpack (prettyToText a) <> "\n\
    --     \\n\
    --     \" <> Text.unpack (Location.renderError "" location)

    displayException (UnboundVariable location name index) =
        "Unbound variable: " <> Text.unpack var <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" location)
        where
            var = prettyToText @(Syntax.Syntax () Void) Syntax.Variable{ location = (), .. }

    displayException (RecordTypeMismatch _A0 _B0 extraA extraB) | extraB == mempty =
        "Record type mismatch\n\
        \\n\
        \The following record type:\n\
        \\n\
        \" <> insert _A0 <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" (locOfCaptured _A0)) <> "\n\
        \\n\
        \… is not a subtype of the following record type:\n\
        \\n\
        \" <> insert _B0 <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" (locOfCaptured _B0)) <> "\n\
        \\n\
        \The former record has the following extra fields:\n\
        \\n\
        \" <> listToText (Map.keys extraA)

    displayException (RecordTypeMismatch _A0 _B0 extraA extraB) | extraA == mempty =
        "Record type mismatch\n\
        \\n\
        \The following record type:\n\
        \\n\
        \" <> insert _A0 <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" (locOfCaptured _A0)) <> "\n\
        \\n\
        \… is not a subtype of the following record type:\n\
        \\n\
        \" <> insert _B0 <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" (locOfCaptured _B0)) <> "\n\
        \\n\
        \The latter record has the following extra fields:\n\
        \\n\
        \" <> listToText (Map.keys extraB)

    displayException (RecordTypeMismatch _A0 _B0 extraA extraB) =
        "Record type mismatch\n\
        \\n\
        \The following record type:\n\
        \\n\
        \" <> insert _A0 <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" (locOfCaptured _A0)) <> "\n\
        \\n\
        \… is not a subtype of the following record type:\n\
        \\n\
        \" <> insert _B0 <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" (locOfCaptured _B0)) <> "\n\
        \\n\
        \The former record has the following extra fields:\n\
        \\n\
        \" <> listToText (Map.keys extraA) <> "\n\
        \\n\
        \… while the latter record has the following extra fields:\n\
        \\n\
        \" <> listToText (Map.keys extraB)

    displayException (UnionTypeMismatch _A0 _B0 extraA extraB) | extraB == mempty =
        "Union type mismatch\n\
        \\n\
        \The following union type:\n\
        \\n\
        \" <> insert _A0 <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" (locOfCaptured _A0)) <> "\n\
        \\n\
        \… is not a subtype of the following union type:\n\
        \\n\
        \" <> insert _B0 <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" (locOfCaptured _B0)) <> "\n\
        \\n\
        \The former union has the following extra alternatives:\n\
        \\n\
        \" <> listToText (Map.keys extraA)

    displayException (UnionTypeMismatch _A0 _B0 extraA extraB) | extraA == mempty =
        "Union type mismatch\n\
        \\n\
        \The following union type:\n\
        \\n\
        \" <> insert _A0 <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" (locOfCaptured _A0)) <> "\n\
        \\n\
        \… is not a subtype of the following union type:\n\
        \\n\
        \" <> insert _B0 <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" (locOfCaptured _B0)) <> "\n\
        \\n\
        \The latter union has the following extra alternatives:\n\
        \\n\
        \" <> listToText (Map.keys extraB)

    displayException (UnionTypeMismatch _A0 _B0 extraA extraB) =
        "Union type mismatch\n\
        \\n\
        \The following union type:\n\
        \\n\
        \" <> insert _A0 <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" (locOfCaptured _A0)) <> "\n\
        \\n\
        \… is not a subtype of the following union type:\n\
        \\n\
        \" <> insert _B0 <> "\n\
        \\n\
        \" <> Text.unpack (Location.renderError "" (locOfCaptured _B0)) <> "\n\
        \\n\
        \The former union has the following extra alternatives:\n\
        \\n\
        \" <> listToText (Map.keys extraA) <> "\n\
        \\n\
        \… while the latter union has the following extra alternatives:\n\
        \\n\
        \" <> listToText (Map.keys extraB)
    -- displayException (UnresolvedMismatch) = "UnresolvedMismatch"
        -- TODO


data TypeInferenceErrorS = TypeInferenceErrorS TypeInferenceError String
    deriving Show

instance Exception TypeInferenceErrorS where
    displayException (TypeInferenceErrorS err s) = displayException err <> "\n" <> Text.unpack (prettyToText s)

buildStack :: StackZipper a -> [Tree a]
buildStack unwrappable@(StackZipper _ (Just _)) = buildStack (unscopeStack unwrappable)
buildStack (StackZipper l1 Nothing) = rev l1
  where
    rev l = rev1 <$> reverse l
    rev1 (Tree x xs) = Tree x $ rev xs

throwErrorStack :: (MonadState Status m, MonadError TypeInferenceErrorS m) => TypeInferenceError -> m a
throwErrorStack err = do
    sM <- State.gets stack
    case sM of
        Just s -> do
            let s' = pushStack "--[aborted]--" s
            throwError $ TypeInferenceErrorS err (Text.unpack $ prettyToText $ showTrees (buildStack s'))
        Nothing -> throwError $ TypeInferenceErrorS err ""
  where
    showTrees trees = mconcat (List.intersperse "\n" (showTree <$> trees))
    showTree (Tree a sub) = Pretty.nest 2 (a <> "\n" <> showTrees sub)

-- Helper functions for displaying errors

locOfCaptured :: CapturedType Type s -> s
locOfCaptured (CapturedType _ _ t) = Type.location t

insert :: Pretty a => a -> String
insert a = Text.unpack (prettyToText ("  " <> Pretty.align (pretty a)))

listToText :: Pretty a => [a] -> String
listToText elements =
    Text.unpack (Text.intercalate "\n" (map prettyEntry elements))
  where
    prettyEntry entry = prettyToText ("• " <> Pretty.align (pretty entry))

prettyToText :: Pretty a => a -> Text
prettyToText = Grace.Pretty.renderStrict False Width.defaultWidth
