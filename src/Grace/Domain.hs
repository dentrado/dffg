module Grace.Domain where
-- {-# LANGUAGE DeriveGeneric      #-}
-- {-# LANGUAGE DeriveLift         #-}
-- {-# LANGUAGE DerivingStrategies #-}
-- {-# LANGUAGE OverloadedStrings  #-}

-- {-| This module exists primarily to avoid a name clash with constructors of the
--     same name in the "Grace.Type" module
-- -}
-- module Grace.Domain
--     ( -- * Domain
--       Domain(..)
--     ) where

-- import Data.Hashable
-- import GHC.Generics (Generic)
-- import Grace.Pretty (Pretty(..), builtin)
-- import Language.Haskell.TH.Syntax (Lift)

-- -- | The domain over which a @forall@ is quantified
-- data Domain t f v
--     = Type t
--     -- ^ @forall (a : Type) . …@
--     | Fields f
--     -- ^ @forall (a : Fields) . …@
--     | Alternatives v
--     -- ^ @forall (a : Alternatives) . …@
--     deriving stock (Eq, Generic, Lift, Show)

-- instance Hashable (Domain t f v)

-- instance Pretty (Domain t f v) where
--     pretty Type         = builtin "Type"
--     pretty Fields       = builtin "Fields"
--     pretty Alternatives = builtin "Alternatives"
